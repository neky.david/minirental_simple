package cz.david.minirental.model.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "address")
public class Address extends BaseEntity {

    @OneToOne(targetEntity = AddressState.class, fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @LazyCollection(LazyCollectionOption.FALSE)
    private AddressState state;

    @NotBlank
    private String street;

    @NotBlank
    private String city;

    @NotBlank
    private String zip;

    public Address() {
    }

    public Address(@NotBlank String street, @NotBlank String city, @NotBlank String zip) {
        this.street = street;
        this.city = city;
        this.zip = zip;
    }

    public String getFormatAddress(){
        return getFormatAddress(false);
    }

    public String getFormatAddress(boolean withState){
        return street + ", " + city + " " + zip + ((withState) ? ", " + state : "");
    }

    public boolean isEmpty() {
        return (street == null || street.isEmpty()) &&
                (city == null || city.isEmpty()) &&
                (zip == null || zip.isEmpty());
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public AddressState getState() {
        return state;
    }

    public void setState(AddressState state) {
        this.state = state;
    }
}
