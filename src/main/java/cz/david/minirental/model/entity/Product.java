package cz.david.minirental.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.david.minirental.helpers.NumberHelper;
import cz.david.minirental.model.enums.ProductCategory;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "product")
public class Product extends BaseEntity {

    @NotNull
    private String name;

    @NotNull
    private double price;

    //Kauce
    private double bail;

    private double lostPrice;

    private boolean shortDiscount;

    private ProductCategory category;

    @OneToMany(targetEntity = Accessory.class, cascade = CascadeType.ALL)
    @Valid
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    //@JoinTable(name = "product_accessories")
    private List<Accessory> accessories;

    @OneToOne(targetEntity = BorrowProduct.class, fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    @JoinColumn(name = "currentBorrowProduct_id")
    private BorrowProduct currentBorrowProduct;

    public Product() {
    }

    public double calcPrice(int days) {
        if (days == -1) {
            return shortDiscount ? price * 0.6 : price;
        } else {
            return price * days;
        }
    }

    /**vymaže přázdné příslušenství (např. při vytvoření produktu)*/
    public void removeEmptyAccessories() {
        if (accessories != null) {
            for (int i = 0; i < accessories.size(); i++) {
                Accessory accessory = accessories.get(i);
                if (accessory.getName() == null || accessory.getName().isEmpty()) {
                    accessories.remove(i);
                    i--;
                }
            }
        }
    }

    /**zašktne se vypůjčené příslušenství*/
    public void checkBorrowedAccessories(BorrowProduct borrowProduct) {
        if (borrowProduct != null) {
            for (int i = 0; i < accessories.size(); i++) {
                Accessory accessory = accessories.get(i);
                boolean isChecked = false;
                if (borrowProduct.getAccessories() != null && borrowProduct.getAccessories().size() > 0) {

                    for (Accessory borrowAccessory :
                            borrowProduct.getAccessories()) {

                        if (accessory.getId().equals(borrowAccessory.getId())) {
                            isChecked = true;
                            break;
                        }
                    }
                }
                accessory.setOffer(isChecked);
            }
        }
    }

    public String getFormatPrice() {
        return NumberHelper.formatDouble(price);
    }

    @JsonIgnore
    public Borrow getCurrentBorrow(){
        return currentBorrowProduct != null ? currentBorrowProduct.getBorrow() : null;
    }

    public static String getProductsIds(List<Product> products) {
        StringBuilder ids = new StringBuilder();
        boolean first = true;

        if (products != null) {
            for (Product product :
                    products) {
                if (!first) {
                    ids.append(",");
                }
                ids.append(product.getId());
                first = false;
            }
        }

        return ids.toString();
    }

    public boolean isAvailable() {
        return currentBorrowProduct == null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getBail() {
        return bail;
    }

    public void setBail(double bail) {
        this.bail = bail;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public List<Accessory> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Accessory> accessories) {
        this.accessories = accessories;
    }

    public BorrowProduct getCurrentBorrowProduct() {
        return currentBorrowProduct;
    }

    public void setCurrentBorrowProduct(BorrowProduct currentBorrowProduct) {
        this.currentBorrowProduct = currentBorrowProduct;
    }

    public double getLostPrice() {
        return lostPrice;
    }

    public void setLostPrice(double lostPrice) {
        this.lostPrice = lostPrice;
    }

    public boolean isShortDiscount() {
        return shortDiscount;
    }

    public void setShortDiscount(boolean shortDiscount) {
        this.shortDiscount = shortDiscount;
    }
}
