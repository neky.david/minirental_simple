package cz.david.minirental.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.david.minirental.helpers.NumberHelper;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.naming.Name;
import javax.persistence.*;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;

@Entity
@Table(name="borrowProduct")
public class BorrowProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(targetEntity = Product.class, fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne(targetEntity = Borrow.class, fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    @JoinColumn(name = "borrow_id")
    private Borrow borrow;

    private double totalPriceWithBonus;
    private double dayPrice;
    private double totalPrice;
    private double bail;
    private boolean shortDiscount;
    private double lostPrice;

    @ManyToMany(targetEntity = Accessory.class, fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    @Valid
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "borrowProduct_accessories")
    private List<Accessory> accessories;

    public BorrowProduct() {
    }

    public BorrowProduct(Product product) {
        this.product = product;
        this.dayPrice = product.getPrice();
        this.bail = product.getBail();
        this.shortDiscount = product.isShortDiscount();
        this.lostPrice = product.getLostPrice();
        this.accessories = product.getAccessories();
    }

    public void fillProductProperties(Product product) {
        this.product = product;
        this.dayPrice = product.getPrice();
        this.bail = product.getBail();
        this.shortDiscount = product.isShortDiscount();
        this.lostPrice = product.getLostPrice();
    }

    //smaže accessories bez ID
    public void removeEmptyAccessories() {
        if (accessories != null) {
            for (int i = 0; i < accessories.size(); i++) {
                Accessory accessory = accessories.get(i);
                if (accessory.getId() == null || accessory.getId() < 1) {
                    accessories.remove(accessory);
                    i--;
                }
            }
        }
    }

    public double calcTotalPrice(int days) {
        //pod 4 hodiny
        if (days == -1) {
            totalPrice = shortDiscount ? dayPrice * 0.6 : dayPrice;
        } else {
            totalPrice = dayPrice * days;
        }
        totalPriceWithBonus = totalPrice + borrow.getBonusPerProduct();
        return totalPrice;
    }

    public String getFormatAccessories() {
        StringBuilder s = new StringBuilder();
        boolean isFirst = true;
        for (Accessory accessory :
                accessories) {
            if (!isFirst)
                s.append(", ");
            else
                isFirst = false;

            s.append(accessory.getName());
        }
        return s.toString();
    }

    public String getFormatDayPrice() {
        return NumberHelper.formatDouble(dayPrice);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getDayPrice() {
        return dayPrice;
    }

    public void setDayPrice(double dayPrice) {
        this.dayPrice = dayPrice;
    }

    public double getBail() {
        return bail;
    }

    public void setBail(double bail) {
        this.bail = bail;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<Accessory> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Accessory> accessories) {
        this.accessories = accessories;
    }

    public Borrow getBorrow() {
        return borrow;
    }

    public void setBorrow(Borrow borrow) {
        this.borrow = borrow;
    }

    public boolean isShortDiscount() {
        return shortDiscount;
    }

    public void setShortDiscount(boolean shortDiscount) {
        this.shortDiscount = shortDiscount;
    }

    public double getLostPrice() {
        return lostPrice;
    }

    public void setLostPrice(double lostPrice) {
        this.lostPrice = lostPrice;
    }

    public double getTotalPriceWithBonus() {
        return totalPriceWithBonus;
    }

    public void setTotalPriceWithBonus(double totalPriceWithBonus) {
        this.totalPriceWithBonus = totalPriceWithBonus;
    }
}
