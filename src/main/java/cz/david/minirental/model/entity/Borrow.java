package cz.david.minirental.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.david.minirental.helpers.CalendarHelper;
import cz.david.minirental.helpers.NumberHelper;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.joda.time.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Entity
@Table(name = "borrow")
public class Borrow extends BaseEntity {

    private boolean reservation;

    private double bonus;

    private double totalPrice;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fromDate;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar toDate;

    @DateTimeFormat(pattern = "dd.MM.yyyy HH.mm.ss")
    @Temporal(TemporalType.TIMESTAMP)
    //datum a čas vrácení produktu
    private Calendar returnDate;

    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @LazyCollection(LazyCollectionOption.FALSE)
    private User user;

    @OneToMany(mappedBy = "borrow", targetEntity = BorrowProduct.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @Fetch(FetchMode.SELECT)
    @Valid
    private List<BorrowProduct> borrowProducts;

    public void removeEmptyAccessories() {
        for (BorrowProduct bProduct:
             borrowProducts) {
            bProduct.removeEmptyAccessories();
        }
    }

    /**nastaví všem BorrowProducts aktuální Borrow*/
    public void setBorrowInBorrowProducts(){
        for (BorrowProduct bProduct :
                borrowProducts) {
            bProduct.setBorrow(this);
        }
    }

    public double getPricePerDay() {
        double price = 0;

        for (BorrowProduct bProduct :
                borrowProducts) {
            price += bProduct.getDayPrice();
        }

        return price;
    }

    public double getBonusPerProduct(){
        return bonus / (borrowProducts.size() > 0 ? borrowProducts.size() : 1);
    }

    public double getDiscountPricePerDay() {
        double price = 0;

        for (BorrowProduct bProduct :
                borrowProducts) {
            if (bProduct.getProduct().isShortDiscount()) {
                price += bProduct.getDayPrice() * 0.6;
            } else {
                price += bProduct.getDayPrice();
            }
        }

        return price;
    }

    /**
     * Vrací částku jaká byla utržena za jeden produkt výpůjčky
     **/
    public double getPriceByProduct(long productId) {
        if(productId > 0 && borrowProducts.size() > 0){
            for (BorrowProduct bProduct : borrowProducts) {
                if (bProduct.getProduct().getId() == productId){
                    return bProduct.getTotalPriceWithBonus();
                }
            }
        }
        return 0;
    }

    /**
     * vypočítá celkovou cenu za výpůjčku
     **/
    public void calcTotalPrice() {
        double total = 0;
        int days = getNumberOfDays();

        for (BorrowProduct bProduct : borrowProducts) {
            total += bProduct.calcTotalPrice(days);
        }

        total += bonus;
        totalPrice = total;
    }

    /**
     * zjístíme počet dní (vrací -1 pokud jde o dobu pod 4 hodiny)
     **/
    public int getNumberOfDays(){
        int days = 0;
        boolean shortTime;

        if (toDate.getTimeInMillis() >= fromDate.getTimeInMillis()) {
            int minutes = Math.abs(Minutes.minutesBetween(LocalDateTime.fromCalendarFields(fromDate), LocalDateTime.fromCalendarFields(toDate)).getMinutes());
            System.out.println("minutes - " + minutes);
            shortTime = minutes <= 240.0;
            if (!shortTime) {
                minutes -= 120;
                days = (int)Math.round((minutes / 1440.0) + 0.5);
                if (days < 1)
                    days = 1;
            }
            else {
                //jde o dobu kratší než 4 hodiny
                days = -1;
            }
        } else {
            throw new IllegalArgumentException("Nelze vypočítat cenu. Datum vytvoření výpůjčky je větší než datum vrácení.");
        }

        return days;
    }

    public double getTotalBail(){
        double bail = 0;

        for (BorrowProduct bProduct :
                borrowProducts) {
            bail += bProduct.getProduct().getBail();
        }
        
        return bail;
    }

    public String getProductsNames() {
        StringBuilder names = new StringBuilder();
        boolean first = true;
        for (BorrowProduct bProduct :
                borrowProducts) {
            if (!first) {
                names.append(", ");
            }
            names.append(bProduct.getProduct().getName());
            first = false;
        }
        return names.toString();
    }

    public List<Accessory> getAccessories(){
        List<Accessory> accessories = new ArrayList<>();

        for (BorrowProduct bProduct :
                borrowProducts) {
            accessories.addAll(bProduct.getAccessories());
        }
        return accessories;
    }

    public boolean isAccessorySelected(Accessory accessory) {
        List<Accessory> accessories = getAccessories();

        if (accessory != null && accessories != null) {
            for (Accessory a :
                    accessories) {
                if (a.getId() != null && accessory.getId() != null &&
                        a.getId().equals(accessory.getId())) {
                    return true;
                }
            }
        }
        return false;
    }


    /***LIST***/

    public static double calcTotalIncome(List<Borrow> borrows) {
        double income = 0;
        for (Borrow b :
                borrows) {
            income += b.getTotalPrice();
        }
        return income;
    }

    public static double getTotalIncomeFromProduct(List<Borrow> borrows, Product product) {
        double income = 0;
        for (Borrow b :
                borrows) {
            income += b.getPriceByProduct(product.getId());
        }
        return income;
    }

    public static Calendar getFirstAvailableDate(List<Borrow> borrows) {
        Calendar date = Calendar.getInstance();
        for (Borrow b : borrows) {
            if (date.before(b.getFromDate())) {
                break;
            } else {
                date = b.getToDate();
                date.add(Calendar.DATE, 1);
            }
        }
        return date;
    }

    //naplní borrowProducts z vložených produktů
    public void setBorrowProductsFromProducts(List<Product> products){
        if(products != null){
            borrowProducts = new ArrayList<>();

            for (Product product :
                    products) {
                borrowProducts.add(new BorrowProduct(product));
            }
        }
    }


    /***HELPERS***/

    public boolean isToDateBeforeNow() {
        return toDate.before(Calendar.getInstance());
    }

    public String getFormatFromDate() {
        return CalendarHelper.getFormatString(fromDate, "dd.MM.yyyy HH:mm");
    }

    public String getFormatFromDate(String format) {
        return CalendarHelper.getFormatString(fromDate, format);
    }

    public String getFormatToDate() {
        return CalendarHelper.getFormatString(toDate, "dd.MM.yyyy HH:mm");
    }

    public String getFormatToDate(String format) {
        return CalendarHelper.getFormatString(toDate, format);
    }

    public String getFormatFromTime() {
        return CalendarHelper.getFormatString(fromDate, "HH:mm");
    }

    public String getFormatToTime() {
        return CalendarHelper.getFormatString(toDate, "HH:mm");
    }

    public String getFormatPrice() {
        return NumberHelper.formatDouble(totalPrice);
    }

    public String getFormatPriceByProduct(long productId){
        return NumberHelper.formatDouble(getPriceByProduct(productId));
    }


    /***GETTERS/SETTERS***/

    public Calendar getFromDate() {
        return fromDate;
    }

    public void setFromDate(Calendar fromDate) {
        this.fromDate = fromDate;
    }

    public Calendar getToDate() {
        return toDate;
    }

    public void setToDate(Calendar toDate) {
        this.toDate = toDate;
    }

    public Calendar getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Calendar returnDate) {
        this.returnDate = returnDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<BorrowProduct> getBorrowProducts() {
        return borrowProducts;
    }

    public void setBorrowProducts(List<BorrowProduct> borrowProducts) {
        this.borrowProducts = borrowProducts;
    }

    public boolean isReservation() {
        return reservation;
    }

    public void setReservation(boolean reservation) {
        this.reservation = reservation;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
}
