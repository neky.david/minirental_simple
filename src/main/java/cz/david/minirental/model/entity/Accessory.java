package cz.david.minirental.model.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "accessory")
public class Accessory extends BaseEntity {
    @NotNull
    private String name;

    private boolean offer;

    public static List<Accessory> generateAccessories(int count){
        List<Accessory> accessories = new ArrayList<>();

        for(int i = 0; i < count; i++){
            Accessory accessory = new Accessory();
            accessory.setOffer(true);
            accessories.add(accessory);
        }

        return accessories;
    }

    public static List<Accessory> getProductsAccessories(List<Product> products){
        List<Accessory> accessories = new ArrayList<>();

        if (products != null && products.size() > 0) {
            for (Product product :
                    products) {
                if(product.getAccessories() != null) {
                    accessories.addAll(product.getAccessories());
                }
            }
        }

        return accessories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOffer() {
        return offer;
    }

    public void setOffer(boolean offer) {
        this.offer = offer;
    }

}
