package cz.david.minirental.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ser.Serializers;
import cz.david.minirental.services.UserService;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
@Table(name = "user")
public class User extends BaseEntity {

    @NotBlank
    private String name;

    @NotBlank
    private String surname;

    private String company;

    @Email
    private String email;

    private String phoneNumber;

    @JsonIgnore
    private String password;

    private String idNumber;

    private String ico;

    private String dic;

    @OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Address address;

    @ManyToMany(targetEntity = Role.class, fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "user_roles")
    @JsonIgnore
    private Set<Role> roles;

    /*@OneToMany(targetEntity = Borrow.class,  mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    private List<Borrow> borrows;*/

    private boolean isRegistered;

    public User() {
    }

    public User(User user) {
        setId(user.getId());
        setCreateDate(user.getCreateDate());
        setUpdateDate(user.getUpdateDate());
        this.name = user.getName();
        this.surname = user.getSurname();
        this.company = user.getCompany();
        this.email = user.getEmail();
        this.phoneNumber = user.getPhoneNumber();
        this.password = user.getPassword();
        this.roles = user.getRoles();
    }

    public static User getCurrentUser() {
        return getCurrentUser(null);
    }

    public static User getCurrentUser(UserService userService, boolean withPassword) {
        User user = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null && !(auth instanceof AnonymousAuthenticationToken)) {
            user = (User) auth.getPrincipal();

            if (userService != null && user != null) {
                user = userService.getById(user.getId()).orElse(null);
            }

            if (user != null && !withPassword) {
                user.setPassword("");
            }
        }
        return user;
    }

    public static User getCurrentUser(UserService userService) {
        return getCurrentUser(userService, false);
    }

    public String getFullName() {
        return name + " " + surname;
    }

    public String getFormatAddress() {
        String s = "";
        if(address != null){
            s = address.getStreet() + ", " + address.getCity() + ", " + address.getZip();
            s = (address.getState() != null) ? s + ", " + address.getState().getName() : s;
        }
        return s;
    }

    /**slouží k rozparsrování celého jména na jednotlivé části (včetně titulů)*/
    public void setFullName(String fullName) {
        String titlesRegex = "(\\w{2,}+\\.( ){1,})|(, \\w+)";

        Pattern pattern = Pattern.compile(titlesRegex);
        Matcher matcher = pattern.matcher(fullName);

        // The substituted value will be contained in the result variable
        String result = matcher.replaceAll("");

        String[] names = result.split(" ");
        if(names.length > 0){
            name = names[0];

            if(names.length > 1){
                surname = names[1];
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(boolean registered) {
        isRegistered = registered;
    }

    /*public List<Borrow> getBorrows() {
        return borrows;
    }

    public void setBorrows(List<Borrow> borrows) {
        this.borrows = borrows;
    }*/

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public String getDic() {
        return dic;
    }

    public void setDic(String dic) {
        this.dic = dic;
    }
}