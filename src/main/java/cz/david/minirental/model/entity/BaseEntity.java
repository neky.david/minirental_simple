package cz.david.minirental.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.david.minirental.helpers.CalendarHelper;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

@MappedSuperclass
public class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    private String guid;

    private boolean isActive;

    @DateTimeFormat(pattern = "dd.MM.yyyy HH.mm.ss")
    private Calendar createDate;

    @DateTimeFormat(pattern = "dd.MM.yyyy HH.mm.ss")
    private Calendar updateDate;

    @JsonIgnore
    public String getFormatCreateDate(){
        return CalendarHelper.getFormatString(createDate, true);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Calendar createDate) {
        this.createDate = createDate;
    }

    public Calendar getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Calendar updateDate) {
        this.updateDate = updateDate;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
