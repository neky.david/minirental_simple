package cz.david.minirental.repository;

import cz.david.minirental.model.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    Page<User> findAllByIsActiveEquals(boolean isActive, Pageable pageable);

    List<User> findAllByIsRegisteredAndIsActiveEquals(boolean isRegistered, boolean isActive);

    List<User> findAllByIsRegisteredEquals(boolean isRegistered);

    Optional<User> findByEmail(String email);

    Optional<User> findByEmailAndIsRegisteredEqualsAndIsActiveEquals(String email, boolean isRegistered, boolean isActive);
}