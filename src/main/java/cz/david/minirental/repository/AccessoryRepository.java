package cz.david.minirental.repository;

import cz.david.minirental.model.entity.Accessory;
import org.springframework.data.repository.CrudRepository;

public interface AccessoryRepository extends CrudRepository<Accessory, Long> {
}
