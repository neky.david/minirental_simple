package cz.david.minirental.repository;

import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.BorrowProduct;
import cz.david.minirental.model.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BorrowProductRepository extends JpaRepository<BorrowProduct, Long> {

    Optional<BorrowProduct> getByBorrowEqualsAndProductEquals(Borrow borrow, Product product);

}
