package cz.david.minirental.repository;

import cz.david.minirental.model.entity.AddressState;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AddressStateRepository extends CrudRepository<AddressState, Long> {

    Optional<AddressState> findByName(String name);
}
