package cz.david.minirental.repository;

import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.enums.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {
    List<Product> getByCategoryAndIsActive(ProductCategory category, boolean isActive);

    Optional<Product> getByIdAndIsActiveEquals(Long id, boolean isActive);

    Optional<Product> getByIdAndCurrentBorrowProductIsNull(Long id);

    List<Product> findAllByIsActive(boolean isActive);
}
