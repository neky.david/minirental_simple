package cz.david.minirental.repository;

import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.BorrowProduct;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

public interface BorrowRepository extends JpaRepository<Borrow, Long>, JpaSpecificationExecutor<Borrow> {

    Optional<Borrow> getByIdAndReservation(Long id, boolean reservation);

    Page<Borrow> getByReservationEqualsAndIsActiveEquals(boolean reservation, boolean isActive, Pageable pageable);

    List<Borrow> getByBorrowProductsOrderByUpdateDateDesc(BorrowProduct borrowProduct);

    List<Borrow> getByUser(User user);

    List<Borrow> getByUserAndFromDateAfterOrderByFromDate(User user, Calendar calendar);

    List<Borrow> getByUserAndFromDateBeforeOrderByFromDate(User user, Calendar calendar);

    @Query("SELECT SUM(b.totalPrice) FROM Borrow b")
    Float getSumOfPrices();

    @Query("SELECT SUM(b.totalPrice) FROM Borrow b WHERE b.fromDate >= :fromCal AND b.toDate <= :toCal")
    Float getSumOfPrices(Calendar fromCal, Calendar toCal);
}
