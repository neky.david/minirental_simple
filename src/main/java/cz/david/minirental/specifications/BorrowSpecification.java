package cz.david.minirental.specifications;

import cz.david.minirental.model.entity.Borrow;
import org.springframework.data.jpa.domain.Specification;

import java.util.Calendar;

public class BorrowSpecification {

    public static Specification<Borrow> isReservation(Boolean reservation) {
        return (Specification<Borrow>) (root, criteria, cb) -> {
            if (reservation != null) {
                return cb.equal(root.get("reservation"), reservation);
            } else {
                return null;
            }
        };
    }

    public static Specification<Borrow> returnDateIsNull() {
        return (Specification<Borrow>) (root, criteria, cb) ->
                cb.isNull(root.get("returnDate"));
    }

    public static Specification<Borrow> productNameLike(String pName, String orderBy) {
        return (Specification<Borrow>) (root, query, cb) -> {
            if (pName != null && !pName.isEmpty()) {
                if(orderBy != null){
                    query.orderBy(cb.desc(root.get(orderBy)));
                }
                return cb.like(cb.lower(root.join("borrowProducts").get("product").get("name")), pName);
            } else {
                return null;
            }
        };
    }

    public static Specification<Borrow> productIdEqual(Long productId, String orderBy) {
        return (Specification<Borrow>) (root, query, cb) -> {
            if(orderBy != null){
                query.orderBy(cb.desc(root.get(orderBy)));
            }
            return cb.equal(root.join("borrowProducts").get("product").get("id"), productId);
        };
    }

    public static Specification<Borrow> userNameLike(String uName) {
        return (Specification<Borrow>) (root, criteria, cb) -> {
            if (uName != null && !uName.isEmpty()) {
                return cb.or(cb.like(cb.lower(root.get("user").get("name")), uName),
                        cb.like(cb.lower(root.get("user").get("surname")), uName));
            } else {
                return null;
            }
        };
    }

}
