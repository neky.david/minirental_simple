package cz.david.minirental.specifications;

import cz.david.minirental.model.entity.Borrow;
import org.springframework.data.jpa.domain.Specification;

import java.util.Calendar;

public class BaseSpecification {

    public static <T> Specification<T> gretherThan(Calendar cal, String key) {
        return (Specification<T>) (root, criteria, cb) -> {
            if (cal != null) {
                return cb.greaterThanOrEqualTo(root.get(key), cal);
            } else {
                return null;
            }
        };
    }

    public static <T> Specification<T> lessThan(Calendar cal, String key) {
        return (Specification<T>) (root, criteria, cb) -> {
            if (cal != null) {
                return cb.lessThanOrEqualTo(root.get(key), cal);
            } else {
                return null;
            }
        };
    }

    public static <T> Specification<T> isInRange(Calendar from, Calendar to, String key) {
        return isInRange(from, to, key, key);
    }

    public static <T> Specification<T> isInRange(Calendar from, Calendar to, String key1, String key2) {
        return (Specification<T>) (root, criteria, cb) -> {
            if (from != null && to != null) {
                return cb.and(cb.greaterThanOrEqualTo(root.get(key1), from),
                        cb.lessThanOrEqualTo(root.get(key2), to));
            } else {
                return null;
            }
        };
    }

    public static <T> Specification<T> isInRange(Calendar date, String key1, String key2) {
        return (Specification<T>) (root, criteria, cb) -> {
            if (date != null) {
                return cb.and(cb.lessThanOrEqualTo(root.get(key1), date),
                        cb.greaterThanOrEqualTo(root.get(key2), date));
            } else {
                return null;
            }
        };
    }

    public static <T> Specification<T> isActive(Boolean active) {
        return (Specification<T>) (root, criteria, cb) -> {
            if (active != null) {
                return cb.equal(root.get("isActive"), active);
            } else {
                return null;
            }
        };
    }

}
