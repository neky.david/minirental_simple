package cz.david.minirental.specifications;

import org.springframework.data.jpa.domain.Specification;

import java.util.Calendar;


public class SpecificationBuilder<T> {

    /*USE*/
    /*SpecificationBuilder<Borrow> sb = new SpecificationBuilder<>();
    sb = sb.where(sb.equal("active", active))
            .and(sb.equal("reservation", reservation))
            .and(sb.calendarGreatherOrEqualThan("createDate", fromCreateDate))
            .and(sb.calendarLessOrEqualThan("createDate", toCreateDate))
            .and(sb.calendarGreatherOrEqualThan("fromDate", fromDate))
            .and(sb.calendarLessOrEqualThan("toDate", toDate))
            .and(sb.like("user.name", "%" + userName + "%"));*/

    private Specification<T> specification;

    public SpecificationBuilder() {
    }

    public SpecificationBuilder(Specification<T> specification) {
        this.specification = specification;
    }

    public SpecificationBuilder<T> where(Specification<T> spec) {
        if (spec != null) {
            specification = spec;
        }
        return new SpecificationBuilder<>(specification);
    }

    public SpecificationBuilder<T> and(Specification<T> addSpec) {
        if (addSpec != null) {
            if (specification != null) {
                specification = specification.and(addSpec);
            } else {
                specification = addSpec;
            }
        }
        return new SpecificationBuilder<>(specification);
    }

    public SpecificationBuilder<T> or(Specification<T> orSpec) {
        if (orSpec != null) {
            if (specification != null) {
                specification = specification.or(orSpec);
            } else {
                specification = orSpec;
            }
        }
        return new SpecificationBuilder<>(specification);
    }

    public Specification<T> equal(String key, Object object) {
        return validAndReturnSpecification((Specification<T>) (root, query, cb) -> cb.equal(root.get(key), object), object);
    }

    public Specification<T> notEqual(String key, Object object) {
        return validAndReturnSpecification((Specification<T>) (root, query, cb) -> cb.notEqual(root.get(key), object), object);
    }

    public Specification<T> like(String key, String string) {
        return validAndReturnSpecification((Specification<T>) (root, query, cb) -> cb.like(cb.lower(root.get(key)), string), string);
    }

    public Specification<T> greaterThan(String key, String string) {
        return validAndReturnSpecification((Specification<T>) (root, query, cb) -> cb.greaterThan(root.get(key), string), string);
    }

    public Specification<T> lessThan(String key, String string) {
        return validAndReturnSpecification((Specification<T>) (root, query, cb) -> cb.lessThan(root.get(key), string), string);
    }

    public Specification<T> calendarGreatherThan(String key, Calendar calendar) {
        return validAndReturnSpecification((Specification<T>) (root, query, cb) -> cb.greaterThan(root.get(key), calendar), calendar);
    }

    public Specification<T> calendarGreatherOrEqualThan(String key, Calendar calendar) {
        return validAndReturnSpecification((Specification<T>) (root, query, cb) -> cb.greaterThanOrEqualTo(root.get(key), calendar), calendar);
    }

    public Specification<T> calendarLessThan(String key, Calendar calendar) {
        return validAndReturnSpecification((Specification<T>) (root, query, cb) -> cb.lessThan(root.get(key), calendar), calendar);
    }

    public Specification<T> calendarLessOrEqualThan(String key, Calendar calendar) {
        return validAndReturnSpecification((Specification<T>) (root, query, cb) -> cb.lessThanOrEqualTo(root.get(key), calendar), calendar);
    }

    private Specification<T> validAndReturnSpecification(Specification<T> specification, Object object) {
        if (object == null) {
            return null;
        } else {
            return specification;
        }
    }

    public Specification<T> getSpecification() {
        return specification;
    }

    public void setSpecification(Specification<T> specification) {
        this.specification = specification;
    }
}
