package cz.david.minirental.specifications;

import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.enums.ProductCategory;
import org.springframework.data.jpa.domain.Specification;

public class ProductSpecification {

    public static Specification<Product> nameLike(String name) {
        return (Specification<Product>) (root, query, cb) -> {
            if (name != null && !name.isEmpty()) {
                return cb.like(cb.lower(root.get("name")), name);
            } else {
                return null;
            }
        };
    }

    public static Specification<Product> categoryEqual(String category) {
        return (Specification<Product>) (root, query, cb) -> {
            if (category != null && !category.isEmpty()) {
                try {
                    ProductCategory cat = ProductCategory.valueOf(category);
                    return cb.equal(root.get("category"), cat);
                } catch (Exception ignored) {
                }
            }
            return null;
        };
    }

    public static Specification<Product> currentBorrowProductIsNull() {
        return (Specification<Product>) (root, criteria, cb) ->
                cb.isNull(root.get("currentBorrowProduct"));
    }

}

