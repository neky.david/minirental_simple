package cz.david.minirental.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.html2pdf.HtmlConverter;
import cz.david.minirental.helpers.CalendarHelper;
import cz.david.minirental.model.entity.Borrow;
import org.joda.time.Days;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
//@RequiredArgsConstructor
public class Html2PdfService {

    private final TemplateEngine templateEngine;

    public Html2PdfService(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public InputStreamResource generateContract(Borrow borrow){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> data = objectMapper.convertValue(borrow, Map.class);

        data.put("isHtml", false);

        long diff = borrow.getToDate().getTimeInMillis() - borrow.getFromDate().getTimeInMillis();
        int daysCount = (int)TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        if(daysCount < 1){
            daysCount = 1;
        }
        data.put("daysCount", daysCount + " " + CalendarHelper.getDayString(daysCount));

        Context context = new Context();
        context.setVariables(data);
        String html = templateEngine.process("pdf/contract", context);

        try {
            OutputStream outputStream = new ByteArrayOutputStream();
            HtmlConverter.convertToPdf(html, outputStream);
            return new InputStreamResource(new ByteArrayInputStream(((ByteArrayOutputStream) outputStream).toByteArray()));
        }
        catch (IOException e){
            e.printStackTrace();
        }

        return null;
    }

}
