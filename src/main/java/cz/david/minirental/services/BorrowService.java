package cz.david.minirental.services;

import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.entity.User;
import cz.david.minirental.helpers.CalendarHelper;
import cz.david.minirental.repository.BorrowRepository;
import cz.david.minirental.specifications.BaseSpecification;
import cz.david.minirental.specifications.BorrowSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class BorrowService implements IBaseService<Borrow> {
    private final BorrowRepository repository;
    private final ProductService productService;

    @Autowired
    public BorrowService(BorrowRepository repository, ProductService productService) {
        this.repository = repository;
        this.productService = productService;
    }

    @Override
    public void save(Borrow b) {
        b.setActive(true);
        b.setCreateDate(Calendar.getInstance());
        b.setUpdateDate(Calendar.getInstance());

        b.setGuid(UUID.randomUUID().toString());

        b.removeEmptyAccessories();
        b.setBorrowInBorrowProducts();
        //b.calcTotalPrice();

        repository.save(b);

        productService.setCurrentBorrowProduct(b);
    }

    @Override
    public void update(Borrow b) {
        Optional<Borrow> origOpt = repository.findById(b.getId());
        if (origOpt.isPresent()) {
            Borrow orig = origOpt.get();

            b.setCreateDate(orig.getCreateDate());
            b.setActive(orig.isActive());
            b.setGuid(orig.getGuid());
            b.setReservation(orig.isReservation());

            b.removeEmptyAccessories();

            b.setUpdateDate(Calendar.getInstance());
            repository.save(b);
        }
    }

    public boolean setReservation(Long id, boolean isReservation) {
        try {
            Optional<Borrow> optB = getById(id);
            if (optB.isPresent()) {
                Borrow b = optB.get();
                b.setReservation(isReservation);
                b.setCreateDate(Calendar.getInstance());
                b.setUpdateDate(b.getCreateDate());
                repository.save(b);
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean returnBorrow(Borrow borrow) {
        Optional<Borrow> optB = getById(borrow.getId());
        if (optB.isPresent()) {
            Borrow b = optB.get();
            if (b.isReservation() || b.getReturnDate() != null) {
                return false;
            }

            b.setToDate(borrow.getToDate());
            b.setReturnDate(Calendar.getInstance());
            b.setUpdateDate(b.getReturnDate());

            b.setBonus(borrow.getBonus());
            b.calcTotalPrice();

            repository.save(b);

            productService.clearCurrentBorrowProduct(b);

            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<Borrow> getAll() {
        return repository.findAll();
    }

    public Page<Borrow> getFiltredBorrows(Boolean active, Boolean reservation, Calendar fromCreateDate, Calendar toCreateDate,
                                          Calendar fromDate, Calendar toDate, String userName, String productName, Pageable pageable) {

        if (productName != null)
            productName = "%" + productName + "%";
        if (userName != null)
            userName = "%" + userName + "%";

        /*System.out.println("getFiltredBorrows:");
        System.out.println(" active - " + active);
        System.out.println(" reservation - " + reservation);
        System.out.println(" fromCreateDate - " + CalendarHelper.getFormatString(fromCreateDate));
        System.out.println(" toCreateDate - " + CalendarHelper.getFormatString(toCreateDate));
        System.out.println(" fromDate - " + CalendarHelper.getFormatString(fromDate));
        System.out.println(" toDate - " + CalendarHelper.getFormatString(toDate));
        System.out.println(" userName - " + userName);
        System.out.println(" productName - " + productName);
        System.out.println(" page - " + pageable.getPageNumber());*/

        Specification<Borrow> specification = Specification
                .where(BorrowSpecification.productNameLike(productName, null)
                        .and(BorrowSpecification.isReservation(reservation))
                        .and(BaseSpecification.isInRange(fromCreateDate, toCreateDate, "createDate"))
                        .and(BaseSpecification.isInRange(fromDate, toDate, "fromDate", "toDate"))
                        .and(BorrowSpecification.userNameLike(userName))
                        .and(BaseSpecification.isActive(active)));

        return repository.findAll(specification, pageable);
    }

    public Page<Borrow> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Page<Borrow> getAllBorrows(Pageable pageable) {
        return repository.getByReservationEqualsAndIsActiveEquals(false, true, pageable);
    }

    public Page<Borrow> getAllReservations(Pageable pageable) {
        return repository.getByReservationEqualsAndIsActiveEquals(true, true, pageable);
    }

    @Override
    public Optional<Borrow> getById(Long id) {
        return repository.findById(id);
    }

    public Optional<Borrow> getReservationById(Long id) {
        return repository.getByIdAndReservation(id, true);
    }

    public Optional<Borrow> getBorrowById(Long id) {
        return repository.getByIdAndReservation(id, false);
    }

    @Override
    public void remove(Long id) {
        Borrow borrow = repository.findById(id).orElse(null);
        if (borrow != null) {
            borrow.setActive(false);
            borrow.setUpdateDate(Calendar.getInstance());
            repository.save(borrow);
        }
    }

    @Override
    public void remove(Borrow b) {
        if (b != null) {
            remove(b.getId());
        }
    }

    public float getSumOfPrices() {
        Float sum = repository.getSumOfPrices();
        return sum == null ? 0 : sum;
    }

    public float getSumOfPrices(Calendar from, Calendar to) {
        Float sum = repository.getSumOfPrices(from, to);
        return sum == null ? 0 : sum;
    }

    public Calendar getFirstAvailableDate(Product product) {
        if (product != null) {
            List<Borrow> borrows = getComingByProduct(product);
            return Borrow.getFirstAvailableDate(borrows);
        }

        return null;
    }

    public List<String> getStringsAvailableDates(List<Product> products) {
        List<String> dates = new ArrayList<>();

        if (products != null && products.size() > 0) {
            for (Product product :
                    products) {
                dates.add(CalendarHelper.getFormatString(getFirstAvailableDate(product), false));
            }
        }
        return dates;
    }

    public List<Borrow> getByProduct(Product product) {
        //return repository.getByProductsOrderByUpdateDateDesc(product);

        Specification<Borrow> specification = Specification
                .where(BorrowSpecification.productIdEqual(product.getId(), null));

        return repository.findAll(specification, Sort.by("updateDate"));
    }

    public List<Borrow> getComingByProduct(Product product) {
        //return repository.getByProductsAndFromDateAfterAndIsActiveOrderByFromDate(product, CalendarHelper.getYesterday(), true);

        Specification<Borrow> specification = Specification
                .where(BorrowSpecification.productIdEqual(product.getId(), "fromDate")
                        .and(BaseSpecification.isActive(true))
                        .and(BaseSpecification.gretherThan(CalendarHelper.getYesterday(), "fromDate")));

        return repository.findAll(specification);

    }

    public List<Borrow> getByUser(User user) {
        return repository.getByUser(user);
    }

    public List<Borrow> getComingByUser(User user) {
        return repository.getByUserAndFromDateAfterOrderByFromDate(user, CalendarHelper.getYesterday());
    }

    public List<Borrow> getPastByUser(User user) {
        return repository.getByUserAndFromDateBeforeOrderByFromDate(user, Calendar.getInstance());
    }

    public String getUnavailableDatesExceptBorrow(Product product, Borrow borrow) {
        if (product != null) {
            StringBuilder builder = new StringBuilder();

            List<Borrow> borrows = getComingByProduct(product);
            if (borrows != null && borrows.size() > 0) {
                builder.append("[");

                int i = 0;
                for (Borrow b :
                        borrows) {

                    //přeskočení zadané borrow
                    if (borrow != null && borrow.getId().equals(b.getId())) {
                        continue;
                    }

                    if (i != 0) {
                        builder.append(", ");
                    }

                    builder.append("[\"").append(CalendarHelper.getFormatString(b.getFromDate(), "yyyy-MM-dd")).append("\", ");
                    builder.append("\"").append(CalendarHelper.getFormatString(b.getToDate(), "yyyy-MM-dd")).append("\"]");
                    i++;
                }
                builder.append("]");
            }
            return builder.toString();
        }

        return "";
    }

    public String getUnavailableDates(Product product) {
        return getUnavailableDatesExceptBorrow(product, null);
    }

    public Optional<Borrow> getCurrentBorrow(Long productId) {

        Specification<Borrow> specification = Specification
                .where(BorrowSpecification.returnDateIsNull()
                        .and(BorrowSpecification.productIdEqual(productId, null))
                        .and(BorrowSpecification.isReservation(false))
                        .and(BaseSpecification.isInRange(Calendar.getInstance(), "fromDate", "toDate"))
                        .and(BaseSpecification.isActive(true)));

        return repository.findOne(specification);
    }
}
