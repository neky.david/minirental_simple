package cz.david.minirental.services;

import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.BorrowProduct;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.repository.BorrowProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BorrowProductService implements IBaseService<BorrowProduct> {

    private final BorrowProductRepository repository;

    @Autowired
    public BorrowProductService(BorrowProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(BorrowProduct bp) {
        repository.save(bp);
    }

    @Override
    public void update(BorrowProduct bp) {
        repository.save(bp);
    }

    @Override
    public List<BorrowProduct> getAll() {
        return repository.findAll();
    }

    @Override
    public Optional<BorrowProduct> getById(Long id) {
        return repository.findById(id);
    }

    @Override
    public void remove(Long id) {
        repository.findById(id).ifPresent(repository::delete);
    }

    @Override
    public void remove(BorrowProduct bp) {
        repository.delete(bp);
    }

    public Optional<BorrowProduct> getByBorrowAndProduct(Borrow borrow, Product product){
        return repository.getByBorrowEqualsAndProductEquals(borrow, product);
    }

    public Optional<BorrowProduct> getByBorrowAndProduct(long borrowId, long productId){
        Borrow borrow = new Borrow();
        borrow.setId(borrowId);
        Product product = new Product();
        product.setId(productId);
        return repository.getByBorrowEqualsAndProductEquals(borrow, product);
    }
}
