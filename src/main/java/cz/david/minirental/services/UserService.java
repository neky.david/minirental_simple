package cz.david.minirental.services;

import cz.david.minirental.helpers.CalendarHelper;
import cz.david.minirental.model.entity.User;
import cz.david.minirental.repository.UserRepository;
import cz.david.minirental.specifications.BaseSpecification;
import cz.david.minirental.specifications.UserSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService implements IBaseService<User> {

    private final UserRepository repository;
    private final RoleService roleService;
    //private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository repository, RoleService roleService) {
        this.repository = repository;
        this.roleService = roleService;
    }

    @Override
    public void save(User u) {
        u.setActive(true);
        u.setCreateDate(Calendar.getInstance());
        u.setUpdateDate(Calendar.getInstance());
        u.setRoles(new HashSet<>(Collections.singletonList(roleService.getDefaultRole().orElse(null))));

        u.setGuid(UUID.randomUUID().toString());

        //smazat prázdnou adresu
        if(u.getAddress() != null && u.getAddress().isEmpty()){
            u.setAddress(null);
        }

        repository.save(u);
    }

    @Override
    public void update(User u) {
        Optional<User> origOpt = repository.findById(u.getId());
        if (origOpt.isPresent()) {

            User orig = origOpt.get();

            u.setActive(orig.isActive());
            u.setRegistered(orig.isRegistered());
            u.setCreateDate(orig.getCreateDate());
            u.setUpdateDate(Calendar.getInstance());
            u.setRoles(orig.getRoles());
            u.setPassword(orig.getPassword());

            //smazat prázdnou adresu
            if(u.getAddress() != null && u.getAddress().isEmpty()){
                u.setAddress(null);
            }

            //address
            if (u.getAddress() != null && orig.getAddress() != null)
                u.getAddress().setId(orig.getAddress().getId());

            repository.save(u);
        }
    }

    @Override
    public List<User> getAll() {
        return repository.findAll();
    }

    public Page<User> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Page<User> getAllActive(Pageable pageable) {
        return repository.findAllByIsActiveEquals(true, pageable);
    }

    public List<User> getAllCustomersByActive(Boolean active) {

        Specification<User> specification = Specification
                .where(UserSpecification.roleEqual("CUSTOMER")
                        .and(BaseSpecification.isActive(active)));

        return repository.findAll(specification);

    }

    public Page<User> getFilteredCustomers(Boolean active, String name, String company, String email, String phone, String address, Pageable pageable) {
        if (name != null)
            name = "%" + name + "%";
        if (company != null)
            company = "%" + company + "%";
        if (email != null)
            email = "%" + email + "%";
        if (phone != null)
            phone = "%" + phone + "%";
        if (address != null)
            address = "%" + address + "%";

        Specification<User> specification = Specification
                .where(UserSpecification.roleEqual("CUSTOMER")
                        .and(BaseSpecification.isActive(active))
                        .and(UserSpecification.nameLike(name))
                        .and(UserSpecification.companyLike(company))
                        .and(UserSpecification.emailLike(email))
                        .and(UserSpecification.phoneNumberLike(phone))
                        .and(UserSpecification.addressLike(address)));

        return repository.findAll(specification, pageable);
    }

    @Override
    public Optional<User> getById(Long id) {
        return repository.findById(id);
    }

    @Override
    public void remove(Long id) {
        User user = repository.findById(id).orElse(null);
        if (user != null) {
            user.setActive(false);
            user.setUpdateDate(Calendar.getInstance());
            repository.save(user);
        }
    }

    @Override
    public void remove(User u) {
        remove(u.getId());
    }

    public Optional<User> getUserByEmail(String email) {
        return repository.findByEmail(email);
    }

    public Optional<User> getActiveUserByEmail(String email) {
        return repository.findByEmailAndIsRegisteredEqualsAndIsActiveEquals(email, true, true);
    }

    public List<User> findByLikeNameOrSurnameOrCompanyOrEmailOrPhone(String value) {
        if (value != null)
            value = "%" + value + "%";

        Specification<User> specification = Specification
                .where(UserSpecification.roleEqual("CUSTOMER")
                        .and(BaseSpecification.isActive(true))
                        .and(UserSpecification.nameLike(value)
                                .or(UserSpecification.companyLike(value))
                                .or(UserSpecification.emailLike(value))
                                .or(UserSpecification.phoneNumberLike(value))));

        return repository.findAll(specification);

    }
}
