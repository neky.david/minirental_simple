package cz.david.minirental.services;

import cz.david.minirental.model.UserDetailImpl;
import cz.david.minirental.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailService implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public UserDetailService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> optionalUser = userService.getActiveUserByEmail(email);

        if(optionalUser.isPresent() && !optionalUser.get().isActive()){
            throw new UsernameNotFoundException("User access denied!");
        }

        optionalUser.orElseThrow(() -> new UsernameNotFoundException("Username not found!!!"));

        return optionalUser.map(UserDetailImpl::new).get();
    }
}
