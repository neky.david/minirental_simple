package cz.david.minirental.services;

import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.BorrowProduct;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.enums.ProductCategory;
import cz.david.minirental.repository.ProductRepository;
import cz.david.minirental.specifications.BaseSpecification;
import cz.david.minirental.specifications.ProductSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class ProductService implements IBaseService<Product> {
    private final ProductRepository repository;

    @Autowired
    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(Product p) {
        p.setActive(true);
        p.setCreateDate(Calendar.getInstance());
        p.setUpdateDate(Calendar.getInstance());

        p.setGuid(UUID.randomUUID().toString());

        p.removeEmptyAccessories();

        repository.save(p);
    }

    @Override
    public void update(Product p) {
        Optional<Product> origOpt = repository.findById(p.getId());
        if (origOpt.isPresent()) {
            Product orig = origOpt.get();

            p.setCurrentBorrowProduct(orig.getCurrentBorrowProduct());
            p.setCreateDate(orig.getCreateDate());
            p.setActive(orig.isActive());
            p.setGuid(orig.getGuid());

            p.removeEmptyAccessories();

            p.setUpdateDate(Calendar.getInstance());
            repository.save(p);
        }
    }

    @Override
    public List<Product> getAll() {
        return repository.findAll();
    }

    public Page<Product> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Optional<Product> getById(Long id) {
        return repository.findById(id);
    }

    public Optional<Product> getAvailableById(Long id) {
        return repository.getByIdAndCurrentBorrowProductIsNull(id);
    }

    public Optional<Product> getByIdAndActive(Long id, boolean active) {
        return repository.getByIdAndIsActiveEquals(id, active);
    }

    public Optional<Product> getActiveById(Long id) {
        return getByIdAndActive(id, true);
    }

    @Override
    public void remove(Long id) {
        Product product = repository.findById(id).orElse(null);
        if (product != null) {
            product.setActive(false);
            product.setUpdateDate(Calendar.getInstance());
            repository.save(product);

        }
    }

    public boolean tryRemove(Long id) {

        Product product = repository.findById(id).orElse(null);
        if (product != null) {
            product.setActive(false);
            product.setUpdateDate(Calendar.getInstance());
            repository.save(product);

            return true;
        }
        return false;
    }

    @Override
    public void remove(Product p) {
        if (p != null) {
            remove(p.getId());
        }
    }

    public Page<Product> getFiltredProducts(boolean active, String name, String category, Pageable pageable) {

        if (name != null)
            name = "%" + name + "%";

        Specification<Product> specification = Specification
                .where(ProductSpecification.nameLike(name)
                        .and(ProductSpecification.categoryEqual(category))
                        .and(BaseSpecification.isActive(active)));

        return repository.findAll(specification, pageable);
    }

    public List<Product> getActiveByCategory(ProductCategory category) {
        return repository.getByCategoryAndIsActive(category, true);
    }

    public List<Product> findByLikeNameOrId(String value, boolean onlyAvailable) {
        List<Product> products = new ArrayList<>();
        try {
            Long id = Long.parseLong(value);
            Optional<Product> optP = getActiveById(id);
            optP.ifPresent(products::add);
        } catch (Exception ignored) {
        }

        if (value != null)
            value = "%" + value + "%";

        Specification<Product> specification;

        if(onlyAvailable) {
            specification = Specification
                    .where(BaseSpecification.<Product>isActive(true)
                    .and(ProductSpecification.currentBorrowProductIsNull())
                    .and(ProductSpecification.nameLike(value)));
        }
        else {
            specification = Specification
                    .where(BaseSpecification.<Product>isActive(true)
                            .and(ProductSpecification.nameLike(value)));
        }

        products.addAll(repository.findAll(specification));

        return products;
    }

    /**nastaví všem produktům z výpůjčky currentBorrowProduct*/
    public void setCurrentBorrowProduct(Borrow borrow) {
        for (BorrowProduct bProduct :
                borrow.getBorrowProducts()) {
            Product product = repository.findById(bProduct.getProduct().getId()).orElse(null);
            if (product != null) {
                product.setCurrentBorrowProduct(bProduct);
                repository.save(product);
            }
        }
    }

    public void clearCurrentBorrowProduct(Borrow borrow){
        for (BorrowProduct bProduct :
                borrow.getBorrowProducts()) {
            Product product = repository.findById(bProduct.getProduct().getId()).orElse(null);
            if (product != null) {
                product.setCurrentBorrowProduct(null);
                repository.save(product);
            }
        }
    }

    public List<Product> getAllActive() {
        return repository.findAllByIsActive(true);
    }
}
