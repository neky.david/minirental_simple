package cz.david.minirental.services;

import cz.david.minirental.model.entity.Role;
import cz.david.minirental.repository.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleService implements IBaseService<Role>  {

    private final RoleRepository repository;

    public RoleService(RoleRepository roleRepository) {
        this.repository = roleRepository;
    }

    @Override
    public void save(Role r) {
        repository.save(r);
    }

    @Override
    public void update(Role r) {
        repository.save(r);
    }

    @Override
    public List<Role> getAll() {
        return (List<Role>)repository.findAll();
    }

    @Override
    public Optional<Role> getById(Long id) {
        return repository.findById(id);
    }

    @Override
    public void remove(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void remove(Role r) {
        repository.delete(r);
    }

    public Optional<Role> getByName(String name) { return repository.findByName(name); }

    public Optional<Role> getDefaultRole(){
        return getByName("CUSTOMER");
    }
}
