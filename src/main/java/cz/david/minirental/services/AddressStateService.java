package cz.david.minirental.services;

import cz.david.minirental.model.entity.AddressState;
import cz.david.minirental.repository.AddressStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressStateService implements IBaseService<AddressState> {

    private final AddressStateRepository repository;

    @Autowired
    public AddressStateService(AddressStateRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(AddressState a) {
        repository.save(a);
    }

    @Override
    public void update(AddressState a) {
        repository.save(a);
    }

    @Override
    public List<AddressState> getAll() {
        return (List<AddressState>) repository.findAll();
    }

    @Override
    public Optional<AddressState> getById(Long id) {
        return repository.findById(id);
    }

    public Optional<AddressState> getByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public void remove(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void remove(AddressState a) {
        repository.delete(a);
    }

}
