package cz.david.minirental.helpers;

import java.text.DecimalFormat;

public class NumberHelper {

    public static String formatFloat(float number) {
        return formatDouble(number);
    }

    public static String formatDouble(double number) {
        if(number == 0){
            return "0,0";
        }
        DecimalFormat df = new DecimalFormat("#,###,###.0");
        String s = df.format(number);
        return s;
    }

}
