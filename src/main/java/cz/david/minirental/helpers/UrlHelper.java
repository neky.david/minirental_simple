package cz.david.minirental.helpers;

public class UrlHelper {

    public static String addParamToUrl(String url, String name, String value){
        if(url.contains("?")){
            return url + "&" + name + "=" + value;
        }
        else {
            return url + "?" + name + "=" + value;
        }
    }

}
