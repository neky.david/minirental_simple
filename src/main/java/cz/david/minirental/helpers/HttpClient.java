package cz.david.minirental.helpers;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.nio.charset.Charset;

public class HttpClient {

    // one instance, reuse
    private static final CloseableHttpClient httpClient = HttpClients.createDefault();

    public static String getString(String url) throws Exception {
        HttpGet request = new HttpGet(url);

        // add request headers
        /*request.addHeader("custom-key", "mkyong");
        request.addHeader(HttpHeaders.USER_AGENT, "Googlebot");*/

        try (CloseableHttpResponse response = httpClient.execute(request)) {

            // Get HttpResponse Status
            //System.out.println(response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();
            Header headers = entity.getContentType();
            //System.out.println(headers);

            if (entity != null) {
                // return it as a String
                return EntityUtils.toString(entity, Charset.forName("UTF-8"));
            }
        }

        return "";
    }
}
