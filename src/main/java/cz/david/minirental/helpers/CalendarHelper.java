package cz.david.minirental.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class CalendarHelper {

    public static Calendar removeTime(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    public static Date addDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 1);
        return cal.getTime();
    }

    public static Calendar getCalendarFromRangeString(String value, int index) {
        if (value != null && !value.isEmpty()) {
            String[] strings = value.split(" - ");
            if (strings.length > index) {
                return getCalendarFromString(strings[index]);
            }
        }
        return null;
    }

    public static Calendar getCalendarFromString(String value) {
        return getCalendarFromString(value, "dd.MM.yyyy");
    }

    public static Calendar getCalendarFromString(String value, String pattern) {
        Calendar cal = null;
        if (value != null && !value.isEmpty()) {
            try {
                cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.GERMANY);
                cal.setTime(sdf.parse(value));
            } catch (Exception ignored) {
                cal = null;
            }
        }
        return cal;
    }

    public static Calendar getCalendarFromStringOrDefault(String value, Calendar defaul) {
        return getCalendarFromStringOrDefault(value, "dd.MM.yyyy", defaul);
    }

    public static Calendar getCalendarFromStringOrDefault(String value, String pattern, Calendar defaul) {
        if(value == null || value.equals("")){
            return defaul;
        }
        Calendar calendar = getCalendarFromString(value, pattern);
        if (calendar == null){
            return defaul;
        }
        return calendar;
    }

    public static String getFormatString(Calendar cal) {
        return getFormatString(cal, "dd.MM.yyyy HH:mm:ss");
    }

    public static String getFormatString(Calendar cal, boolean time) {
        if (time)
            return getFormatString(cal, "dd.MM.yyyy HH:mm:ss");
        else
            return getFormatString(cal, "dd.MM.yyyy");
    }

    public static String getFormatString(Calendar cal, String format) {
        if (cal != null) {
            DateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.format(cal.getTime());
        }
        return "";
    }

    public static String getCurrentTime() {
        return getFormatString(Calendar.getInstance(), "HH:mm");
    }

    public static String getDayString(int dayCount) {
        String day = "dní";
        if (dayCount == 1) {
            day = "den";
        } else if (dayCount > 1 && dayCount < 5) {
           day = "dny";
        }

        return day;
    }

    public static String getCurrentDate() {
        return getFormatString(Calendar.getInstance(), "dd.MM.yyyy");
    }

    public static Calendar getYesterday() {
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        return yesterday;
    }

    public static Calendar addTimeToDate(Calendar calendar, String time) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Calendar calTime = Calendar.getInstance();
        calTime.setTime(sdf.parse(time));
        /*System.out.println("--------------------------------------------------------------");
        System.out.println("input calendar - " + getFormatString(calendar, true));
        System.out.println("input time - " + time);
        System.out.println("**");
        System.out.println("calTime - " + getFormatString(calTime, true));*/
        calendar.add(Calendar.HOUR_OF_DAY, calTime.get(Calendar.HOUR_OF_DAY));
        //System.out.println("HOUR - " + calTime.get(Calendar.HOUR_OF_DAY));
        calendar.add(Calendar.MINUTE, calTime.get(Calendar.MINUTE));
        //System.out.println("MINUTE - " + calTime.get(Calendar.MINUTE));
        calendar.add(Calendar.SECOND, calTime.get(Calendar.SECOND));
        //System.out.println("SECOND - " + calTime.get(Calendar.SECOND));
        //System.out.println("OUTPUT calendar - " + getFormatString(calendar, true));
        return calendar;
    }

    public static String getFormatedToday() {
        return getFormatString(Calendar.getInstance(), false);
    }
}
