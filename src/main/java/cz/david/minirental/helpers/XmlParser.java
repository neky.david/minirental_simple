package cz.david.minirental.helpers;

import cz.david.minirental.model.entity.Address;
import cz.david.minirental.model.entity.AddressState;
import cz.david.minirental.model.entity.User;
import cz.david.minirental.services.AddressStateService;
import org.springframework.stereotype.Service;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class XmlParser {

    private final AddressStateService addressStateService;

    public XmlParser(AddressStateService addressStateService) {
        this.addressStateService = addressStateService;
    }

    public User toUser(String xml) throws SAXException, ParserConfigurationException, IOException {

        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser = parserFactor.newSAXParser();
        UserSaxHandler handler = new UserSaxHandler(addressStateService);
        parser.parse(new InputSource(new ByteArrayInputStream(xml.getBytes(Charset.defaultCharset()))), handler);

        //Printing the list of employees obtained from XML
        /*for (User user : handler.userList) {
            System.out.println(user);
        }*/
        if (handler.userList.size() > 0) {
            return handler.userList.get(0);
        }

        return null;
    }

    public class UserSaxHandler extends DefaultHandler {

        private final AddressStateService addressStateService;

        List<User> userList = new ArrayList<>();
        User user = null;
        String content = null;

        Boolean isPerson = null;
        String name = null;

        public UserSaxHandler(AddressStateService addressStateService) {
            this.addressStateService = addressStateService;
        }

        @Override
        //Triggered when the start of tag is found.
        public void startElement(String uri, String localName,
                                 String qName, Attributes attributes)
                throws SAXException {

            switch (qName) {
                //Create a new Employee object when the start tag is found
                case "are:Zaznam":
                    user = new User();
                    user.setAddress(new Address());
                    isPerson = null;
                    name = null;
                    break;
            }
        }

        @Override
        public void endElement(String uri, String localName,
                               String qName) throws SAXException {
            switch (qName) {
                //Add the employee to list once end tag is found
                case "are:Zaznam":
                    if (name != null) {
                        if (isPerson != null && isPerson) {
                            user.setFullName(name);
                        }
                        user.setCompany(name);
                    }

                    userList.add(user);
                    break;
                //For all other end tags the employee has to be updated.
                case "dtt:Kod_PF":
                    try {
                        int kodPf = Integer.parseInt(content);

                        isPerson = kodPf < 109 || kodPf == 641;
                    } catch (Exception ignored) {
                    }
                    break;
                case "are:Obchodni_firma":
                    name = content;
                    break;
                case "are:ICO":
                    user.setIco(content);
                    break;
                case "dtt:Kod_statu":
                    AddressState state = new AddressState();
                    if (content.equals("203")) {//ČR
                        user.getAddress().setState(addressStateService.getByName("Česká republika").orElse(null));
                    } else if (content.equals("703")) {//SLOVENSKO
                        user.getAddress().setState(addressStateService.getByName("Slovensko").orElse(null));
                    }
                    break;
                case "dtt:Nazev_obce":
                    user.getAddress().setCity(content);
                    break;
                case "dtt:Nazev_ulice":
                    user.getAddress().setStreet(content);
                    break;
                case "dtt:Cislo_domovni":
                    String street = Optional.ofNullable(user.getAddress().getStreet()).orElse(user.getAddress().getCity()); //street or city
                    user.getAddress().setStreet(
                            (street != null ? street + " " : "") + content
                    );
                    break;
                case "dtt:Cislo_orientacni":
                    String str = user.getAddress().getStreet();
                    if(str != null)
                        str = str + (str.matches(".*\\d.*") ? "/" : " /" ); //obsahuje číslo?
                    else
                        str = user.getAddress().getCity() != null ? user.getAddress().getCity() + " /" : "";
                    user.getAddress().setStreet(str + content);
                    break;
                case "dtt:PSC":
                    user.getAddress().setZip(content);
                    break;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length)
                throws SAXException {
            content = String.copyValueOf(ch, start, length).trim();
        }
    }

}
