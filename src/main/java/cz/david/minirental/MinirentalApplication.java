package cz.david.minirental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.annotation.PostConstruct;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.TimeZone;

@SpringBootApplication
@EnableAsync
public class MinirentalApplication {

	public static final String AJAX_HEADER_NAME = "X-Requested-With";
	public static final String AJAX_HEADER_VALUE = "XMLHttpRequest";

	public static void main(String[] args) {
		SpringApplication.run(MinirentalApplication.class, args);
	}

	@PostConstruct
	public void init(){
		// Setting Spring Boot SetTimeZone
		TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of("Europe/Prague")));
	}

}
