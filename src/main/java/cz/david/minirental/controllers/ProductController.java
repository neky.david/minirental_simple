package cz.david.minirental.controllers;

import cz.david.minirental.model.entity.Accessory;
import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.BorrowProduct;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.enums.ProductCategory;
import cz.david.minirental.services.BorrowService;
import cz.david.minirental.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

import static cz.david.minirental.MinirentalApplication.AJAX_HEADER_NAME;
import static cz.david.minirental.MinirentalApplication.AJAX_HEADER_VALUE;

@Controller
public class ProductController extends BaseController {

    private final int PAGE_SIZE = 20;

    private final ProductService productService;
    private final BorrowService borrowService;

    @Autowired
    public ProductController(ProductService productService, BorrowService borrowService) {
        this.productService = productService;
        this.borrowService = borrowService;
    }

    /**
     * LIST - INDEX
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String products(Model model) {

        List<Product> products = productService.getAllActive();

        model.addAttribute("products", products);

        return "product/products";
    }

    /**
     * DETAIL
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/{id}", method = RequestMethod.GET)
    public String detailProduct(@PathVariable("id") Long id,
                                RedirectAttributes redir,
                                Model model) {

        Optional<Product> opt = productService.getById(id);

        if (opt.isPresent()) {
            Product product = opt.get();

            if (!product.isAvailable()) {
                product.checkBorrowedAccessories(product.getCurrentBorrowProduct());
                model.addAttribute("borrow", product.getCurrentBorrowProduct().getBorrow());
            }

            model.addAttribute("product", product);

            List<Borrow> borrows = borrowService.getByProduct(product);
            model.addAttribute("borrows", borrows);
            model.addAttribute("totalIncome", Borrow.getTotalIncomeFromProduct(borrows, product));
            model.addAttribute("borrowsCount", borrows.size());
        } else {
            showError("Nepodařilo se vyhledat produkt s id " + id, redir);
            return "redirect:/";
        }

        return "product/productDetail";
    }

    /**
     * NEW
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/new", method = RequestMethod.GET)
    public String newProduct(@RequestParam(value = "cat", required = false) String category, Model model) {

        Product product = new Product();

        product.setAccessories(Accessory.generateAccessories(3));

        if (category != null && !category.equals("null"))
            product.setCategory(ProductCategory.valueOf(category.toUpperCase()));
        model.addAttribute("product", product);
        return "product/productNew";
    }

    /**
     * ADD
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/add", method = RequestMethod.POST)
    public String addProduct(@Valid @ModelAttribute("product") Product product,
                             @RequestParam(value = "btn") String btn, //next/save
                             Model model,
                             RedirectAttributes redir) {
        try {
            productService.save(product);
            showSuccess("Produkt " + product.getName() + " byl úspěšně přidán", redir);

            if (btn.equals("next")) {
                return "redirect:/admin/product/new";
            } else if (btn.equals("save")) {
                return "redirect:/";
            }
        } catch (Exception ex) {
            showError("Během uložení nového produktu nastala neočekávaná chyba.", model, ex);
        }

        return "/admin/product/new";
    }

    /**
     * EDIT
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/edit/{id}", method = RequestMethod.GET)
    public String editProduct(@PathVariable("id") Long id,
                              Model model) {
        model.addAttribute("product", productService.getById(id).orElse(null));

        return "product/productEdit";
    }

    /**
     * UPDATE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/update", method = RequestMethod.POST)
    public String updateProduct(@Valid @ModelAttribute("product") Product product,
                                Model model) {
        try {
            productService.update(product);

            return "redirect:/admin/product/" + product.getId();

        } catch (Exception ex) {
            showError("Během uložení produktu nastala neočekávaná chyba.", model, ex);
        }

        return "product/productEdit";
    }

    /**
     * REMOVE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/remove/{id}", method = RequestMethod.GET)
    public String removeProduct(@PathVariable("id") Long id,
                                RedirectAttributes redir) {

        if(productService.tryRemove(id)) {
            showSuccess("Produkt byl úspěšně smázán.", redir);
        }
        else {
            showError("Nepodařilo se smazat produkt - nebyl nalezen v databázi.", redir);
        }

        return "redirect:/";
    }

    /**
     * ADD ACCESSORY
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/accessory/add", method = RequestMethod.POST)
    public String addAccessory(Product product, HttpServletRequest request) {
        if (product.getAccessories() == null)
            product.setAccessories(new ArrayList<>());
        Accessory accessory = new Accessory();
        accessory.setOffer(true);
        product.getAccessories().add(accessory);
        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
            // It is an Ajax request, render only #accessories fragment of the page.
            return "product/productNew::#accessories";
        } else {
            // It is a standard HTTP request, render whole page.
            return "product/productNew";
        }
    }

    /**
     * REMOVE ACCESSORY
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/accessory/remove", method = RequestMethod.POST)
    public String removeAccessory(Product product, @RequestParam("index") int index, HttpServletRequest request) {
        if (product.getAccessories() != null)
            product.getAccessories().remove(index);
        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
            return "product/productNew::#accessories";
        } else {
            return "product/productNew";
        }
    }

    /**
     * ADD AUTOCOMPLETE PRODUCT
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/autocomplete/add", method = RequestMethod.POST)
    public String addAutocompleteProduct(Borrow borrow, HttpServletRequest request) {
        if (borrow.getBorrowProducts() == null)
            borrow.setBorrowProducts(new ArrayList<>());
        Product product = new Product();
        borrow.getBorrowProducts().add(new BorrowProduct(product));
        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
            // It is an Ajax request, render only #accessories fragment of the page.
            return "borrow/borrowNew::#productList";
        } else {
            // It is a standard HTTP request, render whole page.
            return "borrow/borrowNew";
        }
    }

    /**
     * REMOVE AUTOCOMPLETE PRODUCT
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/autocomplete/remove", method = RequestMethod.POST)
    public String removeAutocompleteProduct(Borrow borrow, @RequestParam("index") int index, HttpServletRequest request) {
        if (borrow.getBorrowProducts() != null)
            borrow.getBorrowProducts().remove(index);
        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
            // It is an Ajax request, render only #accessories fragment of the page.
            return "borrow/borrowNew::#productList";
        } else {
            // It is a standard HTTP request, render whole page.
            return "borrow/borrowNew";
        }
    }

    /**
     * PRODUCT SEARCH
     **/
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/product/search", method = RequestMethod.POST)
    public String productAutocomplete(String value, Model model) {

        model.addAttribute("products", productService.findByLikeNameOrId(value, false));
        return "product/products::#productList";
    }
}
