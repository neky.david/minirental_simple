package cz.david.minirental.controllers;

import cz.david.minirental.helpers.CalendarHelper;
import cz.david.minirental.model.entity.Accessory;
import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.BorrowProduct;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.services.BorrowService;
import cz.david.minirental.services.ProductService;
import cz.david.minirental.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Controller
public class BorrowController extends BaseController {

    private final int PAGE_SIZE = 20;

    private final BorrowService borrowService;
    private final UserService userService;
    private final ProductService productService;

    @Autowired
    public BorrowController(BorrowService borrowService, UserService userService, ProductService productService) {
        this.borrowService = borrowService;
        this.userService = userService;
        this.productService = productService;
    }

    /**
     * ADMIN LIST BORROWS
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrows", method = RequestMethod.GET)
    public String borrows(@RequestParam(required = false) Integer page,
                          @RequestParam(required = false) String toDate,
                          @RequestParam(required = false) String fromDate,
                          Model model) {

        Pageable pageable = PageRequest.of(page != null ? page : 0, PAGE_SIZE, Sort.by("createDate").descending());

        Calendar fromCal = CalendarHelper.getCalendarFromStringOrDefault(fromDate, CalendarHelper.getCalendarFromString("01.01.2010"));
        Calendar toCal = CalendarHelper.getCalendarFromStringOrDefault(toDate, Calendar.getInstance());

        Page<Borrow> borrowPage = borrowService.getFiltredBorrows(
                true, false, null, null,
                fromCal,
                toCal,
                null, null, pageable);

        model.addAttribute("fromDate", CalendarHelper.getFormatString(fromCal, false));
        model.addAttribute("toDate", CalendarHelper.getFormatString(toCal, false));
        model.addAttribute("totalIncome", borrowService.getSumOfPrices(fromCal, toCal));

        model.addAttribute("totalCount", borrowPage.getTotalElements());
        model.addAttribute("pageNumber", page != null ? page : 0);
        model.addAttribute("pageCount", borrowPage.getTotalPages());
        model.addAttribute("borrows", borrowPage.getContent());

        return "borrow/borrows";
    }


    /**
     * DETAIL
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/detail/{id}", method = RequestMethod.GET)
    public String adminDetailBorrow(HttpServletRequest request,
                                    @PathVariable("id") Long id,
                                    Model model) {
        Borrow borrow = borrowService.getById(id).orElse(null);

        if (borrow == null) {
            showError("Nepodařilo se najít rezervaci s id " + id, model);
            return "borrow/borrowDetail";
        }

        model.addAttribute("borrow", borrow);
        return "borrow/borrowDetail";
    }

    /**
     * NEW
     * productIds - seznam idček produktů oddělených čárkou
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/new", method = RequestMethod.GET)
    public String product(@RequestParam(required = false) String productIds,
                          @RequestParam(required = false) Long userId,
                          Model model) {

        Borrow borrow = new Borrow();
        List<Product> products = new ArrayList<>();

        //PRODUCT
        if (productIds != null) {
            String[] ids = productIds.split(",");
            long id;

            for (String sId :
                    ids) {
                try {
                    id = Long.parseLong(sId);
                } catch (Exception ignored) {
                    continue;
                }

                Optional<Product> opt = productService.getAvailableById(id);
                if (opt.isPresent()) {
                    Product product = opt.get();

                    if (product.isAvailable()) {
                        products.add(product);
                    }
                }
            }
        }

        if (products.size() == 0) {
            products.add(new Product());
        } /*else {
            model.addAttribute("accessories", Accessory.getProductsAccessories(products));
        }*/
        borrow.setBorrowProductsFromProducts(products);

        //USER - vyplní se v případě vytvoření nového uživatele
        if (userId != null && userId > 0)
            borrow.setUser(userService.getById(userId).orElse(null));

        model.addAttribute("borrow", borrow);
        return "borrow/borrowNew";
    }

    /**
     * ADD
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/add", method = RequestMethod.POST)
    public String addBorrow(@Valid @ModelAttribute("borrow") Borrow borrow,
                            BindingResult bindingResult,
                            @RequestParam String fromTime,
                            @RequestParam String toTime,
                            Model model,
                            RedirectAttributes redir) {

        if (bindingResult.hasErrors()) {
            return fillAddErrorModelAndReturnView(model, borrow);
        }

        if (borrow.getUser() == null || !userService.getById(borrow.getUser().getId()).isPresent()) {
            showError("Nepodařilo se vytvořit výpůjčku - nebyl nalezen uživatel", model);
            return fillAddErrorModelAndReturnView(model, borrow);
        }

        for (int i = 0; i < borrow.getBorrowProducts().size(); i++) {
            BorrowProduct bProduct = borrow.getBorrowProducts().get(i);
            Optional<Product> optProduct = productService.getById(bProduct.getProduct().getId());
            if (optProduct.isPresent()) {
                bProduct.fillProductProperties(optProduct.get());
            } else {
                showError("Nepodařilo se vytvořit výpůjčku - nebyl nalezen produkt " + bProduct.getProduct().getName(), model);
                return fillAddErrorModelAndReturnView(model, borrow);
            }
        }

        try {
            borrow.setFromDate(CalendarHelper.addTimeToDate(borrow.getFromDate(), fromTime));
            borrow.setToDate(CalendarHelper.addTimeToDate(borrow.getToDate(), toTime));
        } catch (Exception ex) {
            showError("Nepodařilo se vytvořit výpůjčku - datum nebo čas mají špatný formát", model);
            return fillAddErrorModelAndReturnView(model, borrow);
        }

        //borrow.setBorrowProducts(product);
        borrow.setReservation(false);
        borrowService.save(borrow);

        model.addAttribute("borrow", borrow);

        showSuccess("Výpůjčka produktu " + borrow.getProductsNames() + " byla vytvořena", redir);

        redir.addFlashAttribute("openPrintModal", true);
        return "redirect:/admin/product/" + borrow.getBorrowProducts().get(0).getProduct().getId() + "#contract";
    }

    private String fillAddErrorModelAndReturnView(Model model, Borrow borrow) {
        Product product = productService.getById(borrow.getBorrowProducts().get(0).getProduct().getId()).orElse(null);
        model.addAttribute("product", product);
        model.addAttribute("borrow", borrow);
        return "borrow/borrowNew";
    }

    /**
     * RETURN PAGE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/return/{id}", method = RequestMethod.GET)
    public String returnBorrowPage(@PathVariable("id") Long borrowId,
                                   Model model,
                                   RedirectAttributes redir) {

        Optional<Borrow> optBorrow = borrowService.getById(borrowId); //TODO ERROR vrací zdvojené borrowProducts

        if (optBorrow.isPresent()) {
            Borrow borrow = optBorrow.get();
            //product.setAccessoriesByBorrowProduct(product.getCurrentBorrow());

            model.addAttribute("borrow", borrow);
            return "borrow/borrowReturn";
        } else {
            showError("Nepodařilo se vyhledat výpůjčku", redir);
            return "redirect:/";
        }
    }


    /**
     * RETURN
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/return", method = RequestMethod.POST)
    public String returnBorrow(@Valid @ModelAttribute("borrow") Borrow borrow,
                               @RequestParam String toTime,
                               Model model,
                               RedirectAttributes redir) {

        if (borrow != null) {

            try {
                borrow.setToDate(CalendarHelper.addTimeToDate(borrow.getToDate(), toTime));
            } catch (Exception ex) {
                showError("Nepodařilo se vytvořit výpůjčku - datum nebo čas mají špatný formát", model);
                return fillAddErrorModelAndReturnView(model, borrow);
            }

            if (borrowService.returnBorrow(borrow)) {
                showSuccess("Výpůjčka byla vrácena", redir);
                return "redirect:/";
            } else {
                showError("nepodařilo se vrátit výpůjčku.", redir);
                return "redirect:/admin/borrow/return/" + borrow.getId();
            }
        } else {
            showError("nepodařilo se vrátit výpůjčku. Nepodařilo se vyhledat výpůjčku nebo produkt.", redir);
            return "redirect:/";
        }
    }

    /**
     * RECALC ALL PRICES
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/recalc", method = RequestMethod.GET)
    public String recalcPrices(Model model) {
        try {
            List<Borrow> borrows = borrowService.getAll();
            for (Borrow b : borrows) {
                b.calcTotalPrice();
                borrowService.update(b);
            }
            model.addAttribute("message", "Byli úspěšně přepočítány ceny všech výpůjček");
        } catch (Exception ex) {
            showError("Nepodařilo se přepočítat ceny výpůjček", model, ex);
        }

        return "simplePage";
    }

    /**
     * NEW - REFRESH PRODUCT PART
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/borrow/new/accessories", method = RequestMethod.POST)
    public String getBorrowAccessories(String productIds, Model model) {

        Borrow borrow = new Borrow();
        borrow.setBorrowProducts(new ArrayList<>());

        if (productIds != null) {
            List<Product> products = new ArrayList<>();
            String[] ids = productIds.split(",");
            long id;

            for (String sId :
                    ids) {
                id = Long.parseLong(sId);

                Optional<Product> opt = productService.getActiveById(id);
                if (opt.isPresent()) {
                    Product product = opt.get();
                    borrow.getBorrowProducts().add(new BorrowProduct(product));
                    //accessories.addAll(product.getAccessories());
                }
            }
        }

        //model.addAttribute("accessories", accessories);
        model.addAttribute("borrow", borrow);

        return "borrow/borrowNew :: accessories(borrow = ${borrow})";
    }

}
