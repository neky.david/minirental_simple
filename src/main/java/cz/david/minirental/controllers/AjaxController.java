package cz.david.minirental.controllers;

import cz.david.minirental.helpers.HttpClient;
import cz.david.minirental.helpers.XmlParser;
import cz.david.minirental.model.entity.Product;
import cz.david.minirental.model.entity.User;
import cz.david.minirental.services.AddressStateService;
import cz.david.minirental.services.ProductService;
import cz.david.minirental.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class AjaxController extends BaseController {

    private final UserService userService;
    private final ProductService productService;
    private final AddressStateService stateService;
    private final XmlParser xmlParser;

    @Autowired
    public AjaxController(UserService userService, ProductService productService, AddressStateService stateService, XmlParser xmlParser) {
        this.userService = userService;
        this.productService = productService;
        this.stateService = stateService;
        this.xmlParser = xmlParser;
    }

    /**
     * USER AUTOCOMPLETE
     **/
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/ajax/customer/autocomplete", method = RequestMethod.POST)
    public ResponseEntity<?> customerAutocomplete(String value) {

        List<User> users = userService.findByLikeNameOrSurnameOrCompanyOrEmailOrPhone(value);
        return ResponseEntity.ok(users);
    }

    /**
     * PRODUCT AUTOCOMPLETE
     **/
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/ajax/product/autocomplete", method = RequestMethod.POST)
    public ResponseEntity<?> productAutocomplete(String value) {

        List<Product> products = productService.findByLikeNameOrId(value, true);
        return ResponseEntity.ok(products);
    }

    /**
     * FIND USER BY ICO
     **/
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/ajax/user/find", method = RequestMethod.POST)
    public String userFind(String ico, String rUrl, Model model) {

        model.addAttribute("isNew", true);
        model.addAttribute("states", stateService.getAll());
        model.addAttribute("rUrl", rUrl);

        try {
            String response = HttpClient.getString("http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_std.cgi?ico=" + ico);
            User user = xmlParser.toUser(response);
            model.addAttribute("user", user);
        }
        catch (Exception ex){
            showError("Nepodařilo se vyhledat zákaznía v ARES databázi", model);
            model.addAttribute("user", new User());
        }

        return "user/userNew :: userEdit";
    }

}
