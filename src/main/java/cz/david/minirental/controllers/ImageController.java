package cz.david.minirental.controllers;

import cz.david.minirental.model.entity.Accessory;
import cz.david.minirental.model.entity.Product;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static cz.david.minirental.MinirentalApplication.AJAX_HEADER_NAME;
import static cz.david.minirental.MinirentalApplication.AJAX_HEADER_VALUE;

@Controller
public class ImageController {

    Logger logger = LoggerFactory.getLogger(ProductController.class);

    /**IMAGE UPLOAD*/
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/images/upload", method = RequestMethod.POST)
    public String uploadImages(@RequestParam("images") MultipartFile[] images, Product product, HttpServletRequest request) {



        if (AJAX_HEADER_VALUE.equals(request.getHeader(AJAX_HEADER_NAME))) {
            // It is an Ajax request, render only #accessories fragment of the page.
            return "productEdit::#images";
        } else {
            // It is a standard HTTP request, render whole page.
            return "productEdit";
        }
    }

    @RequestMapping(value = "/image/{name}", produces = MediaType.IMAGE_JPEG_VALUE, method = RequestMethod.GET)
    public @ResponseBody byte[] getImageWithMediaType(
            @PathVariable("name") String name,
            HttpServletRequest request) throws IOException {

        try {
            InputStream in = request.getServletContext().getResourceAsStream("/uploads/" + name);
            return IOUtils.toByteArray(in);
        }
        catch (Exception ex){
            logger.error("MINIRENTAL_LOG: Nepodařilo se najít obrázek " + name, ex);
            return new byte[0];
        }
    }

    /*@RequestMapping(value = "/image/{name}", produces = MediaType.IMAGE_JPEG_VALUE, method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImageAsResponseEntity() {
        HttpHeaders headers = new HttpHeaders();
        InputStream in = servletContext.getResourceAsStream("/WEB-INF/images/image-example.jpg");
        byte[] media = IOUtils.toByteArray(in);
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());

        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
        return responseEntity;
    }*/
}
