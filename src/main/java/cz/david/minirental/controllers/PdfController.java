package cz.david.minirental.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.services.BorrowService;
import cz.david.minirental.services.Html2PdfService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@Controller
public class PdfController {

    private final Html2PdfService pdfService;
    private final BorrowService borrowService;

    public PdfController(Html2PdfService pdfService, BorrowService borrowService) {
        this.pdfService = pdfService;
        this.borrowService = borrowService;
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/contract/{borrowId}", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity contractPdf(@PathVariable("borrowId") Long borrowId){

        Optional<Borrow> optBorrow = borrowService.getById(borrowId);

        if(optBorrow.isPresent()) {
            InputStreamResource resource = pdfService.generateContract(optBorrow.get());
            if (resource != null) {
                return ResponseEntity.ok().body(resource);
            } else {
                return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
            }
        }
        else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/html/contract/{borrowId}", method = RequestMethod.GET)
    public String contractHtml(@PathVariable("borrowId") Long borrowId,
                               Model model){

        Optional<Borrow> optBorrow = borrowService.getById(borrowId);

        if(optBorrow.isPresent()) {
            Borrow borrow = optBorrow.get();
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> data = objectMapper.convertValue(borrow, Map.class);
            model.addAllAttributes(data);
            model.addAttribute("isHtml", true);
            return "pdf/contract";
        }
        return "redirect:/";
    }
}
