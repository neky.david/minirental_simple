package cz.david.minirental.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class BaseController {

    Logger logger = LoggerFactory.getLogger(ProductController.class);

    void showError(String message, Model model, Exception ex) {
        logger.error("MINIRENTAL_LOG: " + message, ex);
        model.addAttribute("errorMessage", message);
    }

    void showError(String message, RedirectAttributes redir, Exception ex) {
        logger.error("MINIRENTAL_LOG: " + message, ex);
        redir.addFlashAttribute("errorMessage", message);
    }

    void showError(String message, Model model) {
        model.addAttribute("errorMessage", message);
    }

    void showError(String message, RedirectAttributes redir) {
        redir.addFlashAttribute("errorMessage", message);
    }

    void showSuccess(String message, Model model) {
        model.addAttribute("successMessage", message);
    }

    void showSuccess(String message, RedirectAttributes redir) {
        redir.addFlashAttribute("successMessage", message);
    }
}
