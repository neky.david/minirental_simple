package cz.david.minirental.controllers;

import cz.david.minirental.helpers.UrlHelper;
import cz.david.minirental.model.entity.Borrow;
import cz.david.minirental.model.entity.User;
import cz.david.minirental.services.AddressStateService;
import cz.david.minirental.services.BorrowService;
import cz.david.minirental.services.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class UserController extends BaseController {

    private final int PAGE_SIZE = 20;

    private final UserService userService;
    private final BorrowService borrowService;
    private final PasswordEncoder passwordEncoder;
    private final AddressStateService stateService;

    public UserController(UserService userService, BorrowService borrowService, PasswordEncoder passwordEncoder, AddressStateService stateService) {
        this.userService = userService;
        this.borrowService = borrowService;
        this.passwordEncoder = passwordEncoder;
        this.stateService = stateService;
    }

    /**
     * LOGIN
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        return "user/login";
    }

    /**
     * LOGOUT
     **/
    @RequestMapping(value = "/menu/logout", method = RequestMethod.POST)
    public String menuLogout(@RequestParam(name = "origUrl") String url) {
        SecurityContextHolder.clearContext();

        return "redirect:" + url;
    }

    /**
     * LIST
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/users", method = RequestMethod.GET)
    public String newProduct(Model model) {

        List<User> users = userService.getAllCustomersByActive(true);

        model.addAttribute("users", users);

        return "user/users";
    }

    /**
     * DETAIL USER
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/user/{id}", method = RequestMethod.GET)
    public String detailUser(@PathVariable("id") Long id,
                             Model model) {
        User user = userService.getById(id).orElse(null);
        model.addAttribute("user", user);

        if(user != null) {
            List<Borrow> borrows = borrowService.getByUser(user);
            model.addAttribute("borrows", borrows);
            model.addAttribute("totalIncome", Borrow.calcTotalIncome(borrows));
            model.addAttribute("borrowsCount", borrows.size());
        }

        return "user/userDetail";
    }

    /**
     * PROFILE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String currentUser(Model model) {

        model.addAttribute("user", User.getCurrentUser(userService));
        model.addAttribute("activeNav", "profile");

        return "user/userEdit";
    }

    /**
     * NEW
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/user/new", method = RequestMethod.GET)
    public String register(@RequestParam(value = "rUrl", required = false) String returnUrl,
                           Model model) {

        model.addAttribute("user", new User());
        model.addAttribute("rUrl", returnUrl);
        model.addAttribute("states", stateService.getAll());

        return "user/userNew";
    }

    /**
     * ADD USER
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/user/add", method = RequestMethod.POST)
    public String addUser(@Valid @ModelAttribute("user") User user,
                          BindingResult bindingResult,
                          @RequestParam(value = "rUrl", required = false) String rUrl,
                          Model model,
                          RedirectAttributes redir) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("states", stateService.getAll());
            model.addAttribute("rUrl", rUrl);
            return "user/userNew";
        }

        try {
            //kotroluje jestli už uživatel neexistuje
            if(!user.getEmail().equals("")) {
                Optional<User> exUser = userService.getUserByEmail(user.getEmail());
                if (exUser.isPresent()) {
                    showError("Uživatel s emailem '" + user.getEmail() + "' již existuje", model);
                    model.addAttribute("states", stateService.getAll());
                    model.addAttribute("rUrl", rUrl);
                    return "user/userNew";
                }
            }

            user.setRegistered(false);
            userService.save(user);

            showSuccess("Zákazník byl úspěšně vytvořen", redir);

            if (rUrl != null) {
                return "redirect:" + UrlHelper.addParamToUrl(rUrl, "userId", user.getId().toString());
            }

            return "redirect:/admin/user/" + user.getId();
        } catch (Exception ex) {
            showError("Nepodařilo se vytvořit uživatele", model, ex);
            model.addAttribute("states", stateService.getAll());
            model.addAttribute("rUrl", rUrl);
            return "user/userNew";
        }
    }

    /**
     * ADMIN PROFILE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/profile", method = RequestMethod.GET)
    public String adminProfile(Model model) {

        model.addAttribute("user", User.getCurrentUser(userService));
        model.addAttribute("activeNav", "admin");
        model.addAttribute("isProfile", true);

        return "user/userEdit";
    }

    /**
     * EDIT USER
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/user/edit/{id}", method = RequestMethod.GET)
    public String editUser(@PathVariable("id") Long id,
                           Model model) {

        model.addAttribute("user", userService.getById(id).orElse(null));
        model.addAttribute("states", stateService.getAll());

        return "user/userEdit";
    }

    /**
     * UPDATE USER
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/user/update", method = RequestMethod.POST)
    public String updateUser(@ModelAttribute("user") @Valid User user,
                             BindingResult bindingResult,
                             Model model,
                             HttpServletRequest request,
                             RedirectAttributes redir) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("states", stateService.getAll());
            return "user/userEdit";
        }

        //kotroluje zda neexistuje uživatel se stejným mailem
        Optional<User> exUser = userService.getUserByEmail(user.getEmail());
        if (exUser.isPresent() && !exUser.get().getId().equals(user.getId())) {
            model.addAttribute("user", user);
            model.addAttribute("states", stateService.getAll());
            model.addAttribute("emailErrorMessage", "Uživatel se stejnou emailovou adresou již existuje");
            return "user/userEdit";
        } else {

            try {
                userService.update(user);

                showSuccess("Uživatel byl úspěšně aktualizován", redir);

                return "redirect:/admin/user/" + user.getId();

            } catch (Exception ex) {
                showError("Nepodařilo se upravit uživatele " + user.getEmail(), model, ex);
                model.addAttribute("states", stateService.getAll());
                model.addAttribute("user", user);
                return "user/userEdit";
            }
        }
    }

    /**
     * REMOVE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/user/remove/{id}", method = RequestMethod.GET)
    public String removeUser(@PathVariable("id") Long id,
                             RedirectAttributes redir) {

        User user = userService.getById(id).orElse(null);
        if (user != null) {
            userService.remove(user);

            showSuccess("Zákazník " + user.getFullName() + " byl úspěšně smázán.", redir);
        } else {
            showError("Nepodařilo se smazat zákazníka - nebyl nalezen v databázi.", redir);
        }

        return "redirect:/admin/users";
    }



    /**
     * CHANGE PASSWORD PAGE
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public String changePasswordPage(Model model) {
        return "user/changePassword";
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public String changePassword(HttpServletRequest request,
                                 @RequestParam(name = "origPassword") String origPassword,
                                 @RequestParam(name = "newPassword") String newPassword,
                                 Model model,
                                 RedirectAttributes redir) {

        try {
            User currentUser = User.getCurrentUser(userService, true);
            if (currentUser != null) {

                //kontrola hesla
                if (passwordEncoder.matches(origPassword, currentUser.getPassword())) {

                    currentUser.setPassword(passwordEncoder.encode(newPassword));
                    userService.update(currentUser);

                    showSuccess("Heslo bylo změněno.", redir);

                } else {
                    showError("Původní heslo je špatně", model);
                    return fillAndReturnChangePasswordModel(origPassword, newPassword, model);
                }
            } else {
                showError("Uživatel nebyl nalezen v databázi", model);
                return fillAndReturnChangePasswordModel(origPassword, newPassword, model);
            }
        }
        catch (Exception ex){
            showError("Nepodařilo se změnit heslo.", model, ex);
            return fillAndReturnChangePasswordModel(origPassword, newPassword, model);
        }

        return "redirect:/";
    }

    private String fillAndReturnChangePasswordModel(String origPwd, String newPwd, Model model){
        model.addAttribute("origPassword", origPwd);
        model.addAttribute("newPassword", newPwd);
        return "user/changePassword";
    }


    /**
     * ENCODE PASSWORD PAGE
     */
    @RequestMapping(value = "/encodePassword", method = RequestMethod.GET)
    public String encodePassword(Model model) {
        return "user/encodePassword";
    }

    @RequestMapping(value = "/encodePassword", method = RequestMethod.POST)
    public String changePassword(@RequestParam(name = "password") String password,
                                 Model model) {

        String encodePassword = passwordEncoder.encode(password);
        model.addAttribute("message", encodePassword);
        showSuccess("Heslo bylo úspěšně zakódováno.", model);
        return "simplePage";
    }


    /**
     * ACIVATE USER
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/user/activate", method = RequestMethod.GET)
    public String activateUser(@RequestParam(required = false) Long id,
                               @RequestParam(required = false) String guid,
                               Model model,
                               RedirectAttributes redir) {

        //pokud se uživ. přihlásí ze stránky /user/active tak ho to přesměruje na index
        if (id == null && guid == null) {
            if (model.containsAttribute("loginError"))
                redir.addFlashAttribute("loginError", true);
            return "redirect:/";
        }

        boolean success = false;

        Optional<User> userOptional = userService.getById(id);

        if (userOptional.isPresent()) {
            User user = userOptional.get();

            if (user.getGuid().equals(guid)) {
                user.setActive(true);
                userService.update(user);
                success = true;
            }
        } else {
            showError("uživateln nebyl nalezen.", model);
        }

        model.addAttribute("success", success);
        return "user/userActive";
    }

    /**
     * USER SEARCH
     **/
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/admin/user/search", method = RequestMethod.POST)
    public String userAutocomplete(String value, Model model) {

        model.addAttribute("users", userService.findByLikeNameOrSurnameOrCompanyOrEmailOrPhone(value));
        return "user/users::#userList";
    }
}
