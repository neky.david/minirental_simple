package cz.david.minirental.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommonController extends BaseController {

    /***/
    /**
     * OFFLINE PAGE
     */
    @RequestMapping(value = "/offline", method = RequestMethod.GET)
    public String offlinePage() {

        return "offline";
    }
}
