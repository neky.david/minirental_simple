/**GENERAL**/

function formValidation() {
// Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
}

formValidation();

jQuery(document).ready(function () {

    feather.replace();

    //tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    /*window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = $('.needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);*/




});


// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

function redirectTo(url) {
    window.location = url;
}

function redirectToPage(pageNumber) {
    $('#filterPageNumber').val(pageNumber);
    $('form#filterForm').submit();
}


function confirmWithData(text, btn) {
    try {
        if (text && btn) {
            var index = 0;

            while (text.includes("{" + index + "}")) {
                var data = btn.getAttribute('data-conf-' + index);
                if (data) {
                    text = text.replace("{" + index + "}", data);
                } else {
                    text = text.replace("{" + index + "}", "");
                }
                index++;
            }

            return confirm(text);
        } else {
            console.error("chyba ve funkci confirmWithData - text nebo event nejsou definovány")
        }

        return false;
    } catch (e) {
        console.error(e.toString());
        return false;
    }
}


/**REGISTRATION**/

function checkPassword(pass, repass, required) {

    if (required) {
        if (pass.val().length < 8) {
            pass[0].setCustomValidity('invalid');
        } else {
            pass[0].setCustomValidity('');
        }
    } else {
        if (pass.val().length > 0 && pass.val().length < 8) {
            pass[0].setCustomValidity('invalid');
        } else {
            pass[0].setCustomValidity('');
        }
    }

    comparePasswords(pass, repass);
}


function comparePasswords(pass, repass, required) {

    if (required) {
        if (repass.val().length < 8 || pass.val() !== repass.val()) {
            repass[0].setCustomValidity('invalid');
        } else {
            repass[0].setCustomValidity('');
        }
    } else {
        if ((pass.val().length > 0 && repass.val().length < 8) || pass.val() !== repass.val()) {
            repass[0].setCustomValidity('invalid');
        } else {
            repass[0].setCustomValidity('');
        }
    }
}

function validateEmail(email, required) {
        var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        if ((email[0].value === "" && !required) || email_regex.test(email.val())) {
            email[0].setCustomValidity('');
        } else {
            email[0].setCustomValidity('invalid');
        }
}


/** SPINNER **/
function showSpinner(title, message) {
    var modal = $('#spinner-modal');

    if (modal.length > 0) {
        $('#spinner-title').html(title);
        $('#spinner-message').html(message);
        //$('#spinner-title').innerHTML = title;
        //$('#spinner-message').innerHTML = message;
        modal.modal('show');
    } else {
        console.error("funkce showSpinner lze volat pouze pokud je nalinkovaný fragment spinner");
    }
}

function hideSpinner() {
    var modal = $('#spinner-modal');

    if (modal.length > 0) {
        modal.modal('hide');
        setTimeout(
            function () {
                $('#spinner-title').html('');
                $('#spinner-message').html('');
            }, 800);
    } else {
        console.error("funkce hideSpinner lze volat pouze pokud je nalinkovaný fragment spinner");
    }
}


/**CLIPBOARD**/
function fallbackCopyTextToClipboard(text) {
    var textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
        var successful = document.execCommand("copy");
        var msg = successful ? "successful" : "unsuccessful";
        console.log("Fallback: Copying text command was " + msg);
    } catch (err) {
        console.error("Fallback: Oops, unable to copy", err);
    }

    document.body.removeChild(textArea);
}

function copyTextToClipboard(text) {
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text);
        return;
    }
    navigator.clipboard.writeText(text).then(
        function () {
            console.log("Async: Copying to clipboard was successful!");
        },
        function (err) {
            console.error("Async: Could not copy text: ", err);
        }
    );
}

function copyInnerHtmlToClipboard(selector) {
    var elem = document.querySelector(selector);
    copyTextToClipboard(elem.innerHTML);
}

/**TOAST**/
function showToast(text) {
    // Get the snackbar DIV
    var x = document.getElementById("toast");
    x.innerText = text;

    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

/**BORROW NEW**/
function getDayName(dayCount) {
    if (dayCount === 1) {
        return "den";
    } else if (dayCount < 5) {
        return "dny"
    } else {
        return "dní"
    }
}

/**zobrazí alert s datumy v kolizi*/
function showCollisionDatesError(dates) {
    var message = "Ve vybraném datumu nelze vypůjčit produkt. V nasledujících datumech je již obsazený: ";

    dates.each(function (index) {
        var time = $(this).data("time");
        var date = moment(time);
        message += date.format('DD.MM.YYYY') + ", ";
    });

    message = message.substring(0, message.length - 2);

    var errorMessage = $('#borrowErrorMessage');
    errorMessage.html(message);
    var error = $('#borrowError');
    error.removeClass('d-none');
    error.addClass('show');

    location.href = "#borrow";
}

/**kontrola kolizí před submit*/
function checkCollisions() {
    var collisionDates = $('.lightpick__day.is-disabled.is-in-range')
    if (collisionDates.length > 0) {
        showCollisionDatesError(collisionDates);
        return false;
    }
    return true;
}

function closeAlert(selector) {
    var error = $(selector);
    error.removeClass('show');
    error.addClass('d-none');
}


/**USER AUTOCOMPLETE**/
function userAutocomplete(inp, hdnInp) {
    /*argumenty - input pro zobrazení jména, hidden input pro user id*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;

        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
            return false;
        }

        $.ajax({
            url: '/ajax/customer/autocomplete',
            type: 'POST',
            data: {value: val}, //JSON.stringify(orderId)
            success: function (data) {
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", inp.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                inp.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < data.length; i++) {
                    var user = data[i];
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    b.classList.add("autocomplete-item");
                    b.innerHTML = "" +
                        "<div class=\"d-flex\">" +
                        " <div>\n" +
                        "  " + highlightText(user.name + " " + user.surname, val) + "\n" +
                        "  <p class=\"small text-muted mb-0\">" + highlightText(user.company, val) + "</p>\n" +
                        " </div>\n" +
                        " <div class=\"ml-auto text-right\">\n" +
                        "  <p class=\"small text-muted mb-0\">" + highlightText(user.email, val) + "</p>\n" +
                        "  <p class=\"small text-muted mb-0\">" + highlightText(user.phoneNumber, val) + "</p>\n" +
                        " </div>\n" +
                        " <input type=\"hidden\" class=\"userNameInput\" value=\"" + user.name + " " + user.surname + "\">\n" +
                        " <input type=\"hidden\" class=\"userIdInput\" value=\"" + user.id + "\">\n" +
                        "</div>";

                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByClassName("userNameInput")[0].value;
                        /*hidden input keep user id*/
                        hdnInp.value = this.getElementsByClassName("userIdInput")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            },
            error: function (a, b, c) {
                console.log(a, b, c);
            }
        });

    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        currentFocus = autocompleteInputKeydownFunc(e, currentFocus);
    });

    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target, inp);
    });
}

/**PRODUCT AUTOCOMPLETE**/
function productAutocomplete(inp, hdnInp) {
    /*argumenty - input pro zobrazení jména, hidden input pro user id*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;

        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
            return false;
        }

        $.ajax({
            url: '/ajax/product/autocomplete',
            type: 'POST',
            data: {value: val}, //JSON.stringify(orderId)
            success: function (data) {
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", inp.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                inp.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < data.length; i++) {
                    var product = data[i];
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    b.classList.add("autocomplete-item");
                    b.innerHTML = "" +
                        "<div class=\"d-flex\">" +
                        " <div>\n" +
                        "  " + highlightText(product.name, val) + "\n" +
                        " </div>\n" +
                        " <div class=\"ml-auto text-right\">\n" +
                        "  <p class=\"small text-muted mb-0\">" + highlightText(product.id, val) + "</p>\n" +
                        " </div>\n" +
                        " <input type=\"hidden\" class=\"productNameInput\" value=\"" + product.name + "\">\n" +
                        " <input type=\"hidden\" class=\"productIdInput\" value=\"" + product.id + "\">\n" +
                        "</div>";

                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByClassName("productNameInput")[0].value;
                        /*hidden input keep user id*/
                        hdnInp.value = this.getElementsByClassName("productIdInput")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();

                        refreshAccessories(hdnInp.value);
                    });
                    a.appendChild(b);
                }
            },
            error: function (a, b, c) {
                console.log(a, b, c);
            }
        });

    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        currentFocus = autocompleteInputKeydownFunc(e, currentFocus);
    });

    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target, inp);
    });
}

function refreshAccessories() {
    var ids = '';

    $('.productHiddenInput').each(function (i, elem) {
       if(i > 0){
           ids += ',';
       }
       ids += elem.value;
    });

    $.ajax({
        url: '/admin/borrow/new/accessories',
        type: 'POST',
        data: {productIds: ids}, //JSON.stringify(orderId)
        success: function (data) {
            $('#accessories').replaceWith(data);
        },
        error: function (a, b, c) {
            console.log(a, b, c);
        }
    });
}

/**AUTOCMPLETE**/
function autocompleteInputKeydownFunc(e, currentFocus) {
    var x = document.getElementById(e.target.id + "autocomplete-list");
    if (x) x = x.getElementsByClassName("autocomplete-item");
    if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        currentFocus = addActive(x, currentFocus);
    } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        currentFocus = addActive(x, currentFocus);
    } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
        } else {
            if (x) x[0].click();
        }
    }

    return currentFocus;
}

function addActive(x, currentFocus) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");

    return currentFocus;
}

function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
    }
}

function closeAllLists(elmnt, inp) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
        if (elmnt) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        } else {
            x[i].parentNode.removeChild(x[i]);
        }
    }
}

function highlightText(text, value) {
    var reg = new RegExp(value.toString(), 'gi');
    var output = text.toString().replace(reg, function (str) {
        return '<b>' + str + '</b>'
    });
    return output;
}
