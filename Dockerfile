FROM openjdk:8-jdk-alpine

#VOLUME src/main/webapp/uploads
RUN mkdir -p src/main/webapp/uploads

COPY src/main/webapp/WEB-INF src/main/webapp/WEB-INF
#COPY src/main/webapp/uploads/default.jpg src/main/webapp/uploads/default.jpg
COPY src/main/resources src/main/resources

COPY target/minirental-0.0.1-SNAPSHOT.jar minirental.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "minirental.jar"]